import * as React from 'react';
import {Fragment} from "react";
import styles from "./modal_window.module.scss";
import Col from "reactstrap/es/Col";

const error = <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M56 40L80 64L64 80L40 56L16 80L0 64L24 40L0 16L16 0L40 24L64 0L80 16L56 40Z" fill="#EC5757"/>
            </svg> ;
    const done = <svg width="100" height="80" viewBox="0 0 100 80" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M100 16L38.47 80L38.46 79.99V80L0 40L15.38 24L38.46 48L84.62 0L100 16Z" fill="#4DCA95"/>
    </svg>;



export const Modal = (props) => {
    return (
        <div className={styles.back}>
            <Col  sm={7} md={6} lg={4} xl={3}   className={styles.wrapper}>
                <div>
                   { props.response ? done : error }
                </div>
                <div>
                    {
                        props.response ?
                            <span style={{color:'#4DCA95'}}>Вакансия успешно опубликована!</span> :
                            <Fragment>
                                <div style={{color:'#EC5757'}}>Вакансия не опубликована,</div>
                                <div style={{color:'#EC5757'}}>попробуйте еще раз!</div>
                            </Fragment>
                    }
                </div>
                <div onClick={()=>{props.parent.setState({showModal: false})}}>
                    <div>
                        { props.response ? 'Продолжить' : 'Повторить'}
                    </div>
                </div>
            </Col>
        </div>
    )
}