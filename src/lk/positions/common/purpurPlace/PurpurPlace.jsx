import * as React from 'react';
import styles from './purpurPlace.module.scss';

export default class PurpurPlace extends React.Component {
    render () {
        return (
            <div className={styles[this.props.class]}>
                <div>{this.props.feedback}</div>
            </div>
        )
    }
}