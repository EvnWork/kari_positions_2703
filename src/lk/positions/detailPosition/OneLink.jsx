import React from "react";
import styles from "./styles/linksBlock.module.scss";
import dot from "../../../assets/images/icons/dot.png";
import DropdownMenu from "reactstrap/es/DropdownMenu";
import DropdownItem from "reactstrap/es/DropdownItem";
import DropdownToggle from "reactstrap/es/DropdownToggle";
import Dropdown from "reactstrap/es/Dropdown";

export class OneLink extends  React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dropdownOpen: false,
        }
    }

    toggle = () => {
        this.setState(prevState => ({
            dropdownOpen: !prevState.dropdownOpen
        }));
    };

    // буфер
    copy = () => {
        const copyToClipboard = () => {
            const aux = document.createElement("input");
            aux.setAttribute("value", this.refs.link.innerHTML);
            document.body.appendChild(aux);
            aux.select();
            document.execCommand("copy");
            document.body.removeChild(aux);
        };
        copyToClipboard();
    };
    // какой-то запрос
    delete = (link) => {
        const arr = this.props.parent.state.links.filter((i)=>{
            return  i.link!==link
        });
        this.props.parent.setState({
            links: arr
        })
    };

    render() {
        return  (<div className={styles.link}>
            <div className={styles.linkTitle}>
                <div> {`${this.props.index+1}. ${this.props.i.title}`} </div>
                <Dropdown
                    direction="left"
                    isOpen={this.state.dropdownOpen}
                    toggle={this.toggle}
                >
                    <DropdownToggle className={styles.toggle}>
                        <img src={dot} alt={'icon'}/>
                    </DropdownToggle>
                    <DropdownMenu
                        className={`${styles.modalLink} ${
                            this.props.parent.state.links.length===(this.props.index+1) ? styles.addMargin : ''
                            }`}
                    >
                        <DropdownItem className={styles.dropItem} onClick={this.copy}>Копировать</DropdownItem>
                        <DropdownItem className={styles.dropItem} onClick={()=>this.delete(this.props.i.link)}>Удалить</DropdownItem>
                    </DropdownMenu>
                </Dropdown>
            </div>
            <div className={styles.linkBody} ref={'link'}> {`   ${this.props.i.link}`} </div>

        </div>)
    }

}