import arrowToBack from '../../../assets/images/icons/arrow_left [#361].png'
import React from "react";
import PurpurPlace from "../common/purpurPlace/PurpurPlace";
import styles from "./styles/headerPosition.module.scss";

const WHITE_MAN = <svg width="7" height="18" viewBox="0 0 7 18" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M6.72217 6.01827V7.53839V10.8889C6.72217 11.4007 6.31612 11.8195 5.81984 11.8195H5.47395V16.2093C5.47395 16.7211 5.06791 17.14 4.57163 17.14H2.15039C1.65411 17.14 1.24807 16.7211 1.24807 16.2093V11.804H0.902176C0.405898 11.804 -0.00014782 11.3854 -0.00014782 10.8733V7.53854V6.01842C-0.00014782 5.50655 0.405898 5.08774 0.902176 5.08774H5.81984C6.33116 5.08758 6.72217 5.50655 6.72217 6.01827Z" fill="#D8DAE0"/>
    <path d="M3.3686 4.31218C2.21412 4.31218 1.27824 3.34687 1.27824 2.15609C1.27824 0.965314 2.21412 0 3.3686 0C4.52307 0 5.45895 0.965314 5.45895 2.15609C5.45895 3.34687 4.52307 4.31218 3.3686 4.31218Z" fill="#D8DAE0"/>
</svg>;

    const PURPUR_MAN = <svg width="7" height="18" viewBox="0 0 7 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M6.72217 6.01827V7.53839V10.8889C6.72217 11.4007 6.31612 11.8195 5.81984 11.8195H5.47395V16.2093C5.47395 16.7211 5.06791 17.14 4.57163 17.14H2.15039C1.65411 17.14 1.24807 16.7211 1.24807 16.2093V11.804H0.902176C0.405898 11.804 -0.00014782 11.3854 -0.00014782 10.8733V7.53854V6.01842C-0.00014782 5.50655 0.405898 5.08774 0.902176 5.08774H5.81984C6.33116 5.08758 6.72217 5.50655 6.72217 6.01827Z" fill="#991E66"/>
        <path d="M3.3686 4.31218C2.21412 4.31218 1.27824 3.34687 1.27824 2.15609C1.27824 0.965314 2.21412 0 3.3686 0C4.52307 0 5.45895 0.965314 5.45895 2.15609C5.45895 3.34687 4.52307 4.31218 3.3686 4.31218Z" fill="#991E66"/>
    </svg>;



export const  HeaderPosition = (props) => {

    const goToBack = () => {
        props.context.setState({
            currentPosition: null
        })
    };

    const {name, feedback, busyPositions, needPosition} = props.position;

    const renderLittleMen = () => {
        const arr = [];
        let i = 0;
        while (i<needPosition) {
            arr.push(<div key={i+'a'}>{WHITE_MAN}</div>);
            ++i;
        }
        i = 0;
        while (i<busyPositions) {
             arr.push(<div key={i+'b'}>{PURPUR_MAN}</div>);
            ++i;
        }
        return arr;
    };

    return (
        <div className={styles.header}>
            <div onClick={goToBack} className={styles.wrapperArrow}>
                <img src={arrowToBack} alt={'back'}/>
            </div>
            <div className={styles.wrapperName}>
                <div>{name}</div>
                <div className={styles.wrapperRound}>
                    <PurpurPlace feedback={feedback} class={'smallRound'}/>
                </div>
            </div>
            <div className={styles.wrapperLittleMen}>
                <div>
                    {renderLittleMen().map(i=>i)}
                </div>
            </div>
        </div>
    );
}