import React, {Fragment} from "react";
import styles from "./styles/description.module.scss";
import arrowDown from "../../../assets/images/icons/arrow_down.png";
import arrowTop from  "../../../assets/images/icons/arrow_top.png";
import Col from "reactstrap/es/Col";

export class Description extends  React.Component {

    constructor(props){
        super(props);
        this.state = {
            open: false,
            comment: '',
            warning: false,
            edit: !! this.props.position.addText,
            focusInput: false
        }
    }

    opening = () => {
        this.setState({
            open: !this.state.open
        })
    };

    openClass = (returnValue) =>{
        return this.state.open ? returnValue : ''
    };
    /* evg: Не знаю в каком виде фронт должен получать текст описания,
     поэтому представил его как массив абзацев с заголовком и такстом */
    renderDescriptionBlocks = () => {

        return (<div>
            {
                this.props.position.description.map((i,index)=>{
                return (
                <Fragment key={index}>
                {i.title ? (<div className={styles.title}>
                    {i.title}
                </div>) : false}
                {i.body ? (<div className={styles.text}>
                    {i.body}
                </div>) : false}
                </Fragment>
                )
            })
            }
            { this.state.edit ? (<div className={styles.borderText}>
                {this.props.position.addText}
            </div>) : false}
        </div>)

    };

    renderAddingDescription = () => {
        return this.state.open ?
            (<Fragment>
               { !this.state.edit ?
                 (<input
                onChange={(e)=>this.setState({comment: e.target.value, warning: false})}
                className={`${styles.input} ${this.state.warning?styles.warning:''}`}
                placeholder={this.state.warning ? 'Это обязательное поле для заполнения': 'Добавить описание'}
                defaultValue={this.props.position.addText}
                autoFocus={this.state.focusInput}
               />)
               : false
               }
              <div
                className={styles.saveButton}
                onClick={ ()=> {
                            !this.state.edit ? this.sendNewComment() : this.changeCurrentComment();
                            if (this.state.edit) this.setState({focusInput: true})
                        }}
                >
                {this.state.edit ? 'Изменить' : 'Сохранить'}
            </div>
            </Fragment>)  : false
    };

    sendNewComment = () => {
        // evg: здесь отправляем новый коммент на сервер или в стор
        this.state.comment  ?
            (()=>{
                this.props.position.addText = this.state.comment;
                this.setState({edit: true});
            })()
            : this.setState({warning: true});
    };

    changeCurrentComment = () => {
        this.setState({comment: this.props.position.addText});
        this.setState({edit: false});
    };

    render() {
        return (
            <Col xl={5}  className={`${styles.wrapper} ${this.openClass(styles.openWrapper)}`}>
                <div className={styles.header}>Описание вакансии</div>
                <div className={`${styles.bodyWrapper} ${this.openClass(styles.openWrapper)}`}>
                    <div className={styles.bodyText}>
                        {this.renderDescriptionBlocks()}
                    </div>
                    {this.renderAddingDescription()}
                </div>
                {/*that's below is a decoration*/}
                    {!this.state.open ? (<div className={styles.eclipse}/> ) : false}
                            <div className={styles.wrapperRound}>
                                <div
                                    className={styles.whiteRound }
                                    onClick={this.opening}
                                >
                                    <img src={!this.state.open ? arrowDown : arrowTop} alt={'down'}/>
                                </div>
                            </div>
            </Col>
        )
    }
}