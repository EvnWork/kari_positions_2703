import React from "react";
import styles from "./styles/linksBlock.module.scss";
import dot from "../../../assets/images/icons/dot.png";
import Col from "reactstrap/es/Col";
import {OneLink} from "./OneLink";

export class LinksBlock extends  React.Component {

    constructor(props){
        super(props);
        this.state = {
            comment: '',
            warning: false,
            links: this.props.links
        }
    }


    createNewLink = () => {
        // evg: тут наверное какой то запрос к серверу на генерацию ссылки
        this.state.comment ?
            this.state.links.push({
            title: this.state.comment,
            link: 'https://какая-то сслылка'
              })
            :
            this.setState({
                warning: true
            });
        this.setState({comment: ''});
    };

     mapLinks = (links) => {
        return links.map((i, index)=>{
                return <OneLink
                    key={index}
                    i={i}
                    index={index}
                    links={this.state.links}
                    parent = {this}
                    />
            })
    };

    render () {
        return (
            <Col xl={3} sm={5} className={styles.wrapper}>
                <div className={styles.title}>Сгенерировать ссылку</div>
                <div
                    className={styles.button}
                    onClick={this.createNewLink}
                >
                    Добавить ссылку
                </div>
                <input
                    value={this.state.comment}
                    className={`${styles.input} ${this.state.warning?styles.warning:''}`}
                    placeholder={this.state.warning?'Это обязательное поле для заполнения':'Комментарий к ссылке'}
                    onChange={(e)=>this.setState({comment: e.target.value, warning: false})}
                />
                <div className={styles.title}>Сгенерированные ссылки:</div>
                {
                    this.mapLinks(this.state.links)
                }
            </Col>
        )
}

}

