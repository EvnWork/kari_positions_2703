import * as React from 'react';
import {Position} from './Position';
import styles from './positionWindow.module.scss';
import Col from "reactstrap/es/Col";
import {HeaderPosition} from "./detailPosition/HeaderPosition";
import {Description} from "./detailPosition/Description";
import {PublishBlock} from "./detailPosition/PublishBlock";
import {LinksBlock} from "./detailPosition/LinksBlock";
import {Modal} from "./common/modalWindow/Modal";


export default class PositionsWindow extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentPosition: null,
            showModal: false,
            isSuccessRequest: false
        }
    }

    data  = getPostsData();

    renderPositionBricks = () => {
        return (
            <Col className={styles.wrapper}>
                {
                    this.data.map((i, index)=>{
                        return <Position i={i} key={index}  parent={this}/>
                    })
                }
            </Col>
        )
    };

    render () {
            return !this.state.currentPosition ?
                this.renderPositionBricks() :
                (<div className={styles.wrapper} style={{justifyContent:'space-between'}}>
                    <HeaderPosition position={this.state.currentPosition} context={this}/>
                    <Description position={this.state.currentPosition}/>
                    <PublishBlock parent={this}/>
                    <LinksBlock links={this.state.currentPosition.links}/>
                    {
                        this.state.showModal ?
                            // evg: аргументом передается статус успеха запроса на публикацию
                            <Modal parent={this} response={this.state.isSuccessRequest}/> : false
                    }
                </div>)
    }
}


//  evg: гипотетический массив с объектами должностей
export function getPostsData ()  {
    return [
        {
            name: 'Директор магазина',
            needPosition: 1,
            busyPositions: 1,
            allPositions: 2,
            feedback: 0,
            addText: '',
            description:  [
                {
                    title: '',
                    body: 'Федеральная сеть магазинов обуви и аксессуаров kari приглашает'+
                        'на работу сотрудника, в нашей компании мы хотим видеть'+
                        'хорошего человека. Для нас важен не только Ваш опыт и ' +
                        'образование, сколько личные качества и желание работать.'
                },
                {
                    title:'Обязанности:',
                    body: 'Продавать обувь.'
                },
                {
                    title:'Условия:',
                    body: 'Заработная плата оклад+премия'
                }

            ],
            links: [
                {
                    title: 'Для магазинов',
                    link: 'https://is.gd/342rsdf'
                },
                {
                    title: 'Для QR-кода на листовки',
                    link: 'https://is.gd/sddfFDF3'
                }
            ]
        },
        {
            name: 'Администратор Магазина',
            needPosition: 1,
            busyPositions: 0,
            allPositions: 1,
            feedback: 0,
            addText: '',
            description:  [
                {
                    title: '',
                    body: 'Федеральная сеть магазинов обуви и аксессуаров kari приглашает'+
                        'на работу сотрудника, в нашей компании мы хотим видеть'+
                        'хорошего человека. Для нас важен не только Ваш опыт и ' +
                        'образование, сколько личные качества и желание работать.'
                },
                {
                    title:'Обязанности:',
                    body: 'Продавать обувь.'
                },
                {
                    title:'Условия:',
                    body: 'Заработная плата оклад+премия'
                }

            ],
            links: [
                {
                    title: 'Для магазинов',
                    link: 'https://is.gd/342rsdf'
                },
                {
                    title: 'Для QR-кода на листовки',
                    link: 'https://is.gd/sddfFDF3'
                }
            ]
        },
        {
            name: 'Продавец ювелирных изделий',
            needPosition: 5,
            busyPositions: 2,
            allPositions: 7,
            feedback: 1,
            addText: 'ПРИВЕТ',
            description: [
                {
                    title: '',
                    body: 'Федеральная сеть магазинов обуви и аксессуаров kari приглашает'+
                        'на работу сотрудника, в нашей компании мы хотим видеть'+
                        'хорошего человека. Для нас важен не только Ваш опыт и ' +
                        'образование, сколько личные качества и желание работать.'
                },
                {
                    title:'Обязанности:',
                    body: 'Продавать обувь.'
                },
                {
                    title:'Условия:',
                    body: 'Заработная плата оклад+премия'
                }

            ],
            links: [
                {
                    title: 'Для магазинов',
                    link: 'https://is.gd/342rsdf'
                },
                {
                    title: 'Для QR-кода на листовки',
                    link: 'https://is.gd/sddfFDF3'
                }
            ]
        },
        {
            name: 'Продавец-консультант',
            needPosition: 4,
            busyPositions: 3,
            allPositions: 7,
            feedback: 2,
            addText: '',
            description: [
                {
                    title: '',
                    body: 'Федеральная сеть магазинов обуви и аксессуаров kari приглашает'+
                          'на работу сотрудника, в нашей компании мы хотим видеть'+
                          'хорошего человека. Для нас важен не только Ваш опыт и ' +
                        'образование, сколько личные качества и желание работать.'
                },
                {
                    title:'Обязанности:',
                    body: 'Продавать обувь.'
                },
                {
                    title:'Условия:',
                    body: 'Заработная плата оклад+премия'
                }

            ],
            links: [
                {
                    title: 'Для магазинов',
                    link: 'https://is.gd/342rsdf'
                },
                {
                    title: 'Для QR-кода на листовки',
                    link: 'https://is.gd/sddfFDF3'
                }
            ]
        }
    ];
}
