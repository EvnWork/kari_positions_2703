import * as React from 'react';
import styles from './positionWindow.module.scss';
import Col from "reactstrap/es/Col";
import tie from '../../assets/images/icons/position_icons/tie.png';
import diamond from '../../assets/images/icons/position_icons/diamond.png';
import shop_center from '../../assets/images/icons/position_icons/shop_center.png';
import PurpurPlace from "./common/purpurPlace/PurpurPlace";


export  const Position = (props) => {

    function getIconByName(name) {
        switch (name) {
            case 'Директор магазина':return tie;
            case 'Продавец ювелирных изделий': return diamond;
            case 'Администратор Магазина': return shop_center;
            /**   можно какую-нибудь дефолтную иконку на случай появления
             новой должности, к которой еще нет специфической иконки **/
            default: return tie;
        }
    }

    return (
        <Col
            className={styles.position}
            onClick={() => props.parent.setState({
                currentPosition: props.i
            })}
        >
            <div className={styles.iconBox}>
               <img src={ getIconByName(props.i.name)} alt={'icon'}/>
            </div>
            <div className={styles.infoBox}>
                <div className={styles.header}>
                    <span>{props.i.name}</span>
                </div>
                <div className={styles.info}>
                    <span>{`Работает ${props.i.busyPositions} из ${props.i.allPositions}`}</span>
                </div>
                {
                    props.i.feedback ?
                        (<PurpurPlace
                            feedback={props.i.feedback}
                            class={'bigRound'}
                        />) :
                        false
                }
            </div>
        </Col>
    )
}