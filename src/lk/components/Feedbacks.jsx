import * as React from 'react'
import axios from "axios"
import server from '../../serverConfig'
import { Row, Col, Button } from 'reactstrap'
import Preload from '../../sugarComponents/Preloader'
// import userpic from '../../assets/images/default-userpic.png'
import * as PropTypes from 'prop-types';
import 'moment/locale/ru'
import moment from 'moment'
import { Russian } from "flatpickr/dist/l10n/ru.js";
import Flatpickr from 'react-flatpickr';
import confirmDatePlugin from 'flatpickr/dist/plugins/confirmDate/confirmDate'
import 'flatpickr/dist/plugins/confirmDate/confirmDate.css'
moment.locale('ru')

const ICONS = {
  'phone' : <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M5.99937 1.80001C5.99937 1.46881 5.73057 1.20001 5.39937 1.20001H1.79937C1.46817 1.20001 1.19937 1.46881 1.19937 1.80001V10.2C1.19937 10.5312 1.46817 10.8 1.79937 10.8H5.39937C5.73057 10.8 5.99937 10.5312 5.99937 10.2V1.80001ZM7.2 1.2V10.8C7.2 11.463 6.663 12 6 12H1.2C0.537 12 0 11.463 0 10.8V1.2C0 0.537 0.537 0 1.2 0H6C6.663 0 7.2 0.537 7.2 1.2ZM4.21307 9.00001C4.21307 9.33121 3.94427 9.60001 3.61307 9.60001C3.28187 9.60001 3.01307 9.33121 3.01307 9.00001C3.01307 8.66881 3.28187 8.40001 3.61307 8.40001C3.94427 8.40001 4.21307 8.66881 4.21307 9.00001Z" fill="#7F8184"/></svg>,
  'email' : <svg width="11" height="12" viewBox="0 0 11 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M7.2 8.4H5.4C4.4058 8.4 3.6 7.5942 3.6 6.6V5.4C3.6 4.4058 4.4058 3.6 5.4 3.6C6.3942 3.6 7.2 4.4058 7.2 5.4V8.4ZM5.4 2.4C3.7434 2.4 2.4 3.7434 2.4 5.4V6.6C2.4 8.2566 3.7434 9.6 5.4 9.6H8.4C9.7254 9.6 10.8 8.5254 10.8 7.2V5.4C10.8 2.4174 8.3826 0 5.4 0C2.4174 0 0 2.4174 0 5.4V6.6C0 9.5826 2.4174 12 5.4 12H10.2C10.5312 12 10.8 11.7312 10.8 11.4C10.8 11.0688 10.5312 10.8 10.2 10.8H5.4C3.0804 10.8 1.2 8.9196 1.2 6.6V5.4C1.2 3.0804 3.0804 1.2 5.4 1.2C7.7196 1.2 9.6 3.0804 9.6 5.4V7.2C9.6 7.863 9.0624 8.4 8.4 8.4V5.4C8.4 3.7434 7.0566 2.4 5.4 2.4Z" fill="#7F8184"/></svg>
}
console.log(moment.locale('ru'))

export default class Feedback extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      feedbackList : null
    }
  }

  async componentDidMount(){
    const authUser = JSON.parse(localStorage.getItem('authUser'))
    // console.log(token);

    let login = authUser ? authUser.expiresIn * 1000 > new Date().getTime() : null
    if ( login ) {
      const feedbackList = await axios.get(
        `${server.url}/api/v1/questionnaire`, 
        {
          headers: { 'Authorization' : `Bearer ${authUser.token}` }
        }
      )
      this.setState({
        feedbackList : feedbackList.data
      })
    // } else {
    //   let newLogin = await axios.get(
    //     `${server.url}/api/v1/refresh`,
    //     {
    //       headers: {
    //         'Authorization': `Bearer ${authUser.refreshToken}`,
    //         'Content-Type' : 'application/json'
    //       }
    //     }
    //   )
    //   localStorage.setItem('authUser', JSON.stringify(newLogin.data))
    }

    console.log(this.state.feedbackList)
  }

  render(){

    const { feedbackList } = this.state;

    if ( feedbackList !== null ) {
      return(
        <React.Fragment>
          <div className="feedback-list">
            <div className="feedback-list__header">Отклики на вакансии</div>
            <div className="feedback-list__body">
              {feedbackList.map((item, i) => {
                const info = item.info
                return(
                  <FeedbackItem 
                    key={item.id}
                    info={{...info}}
                    onClick={()=>{this.props.history.push('/protected/feedback/' + item.id)}}
                    />
                )
              })}
            </div>
          </div>
        </React.Fragment>
      )
    } else {
      return <React.Fragment><Preload /></React.Fragment>
    }
  }
}

export const FeedbackItem = props => {
  return (
    <React.Fragment>
      <div className="feedback">
        <div className="feedback__header d-flex">
          { props.userpic && <img src={props.userpic} alt='userpick' className="feedback__userpic" onClick={props.onClick} style={{cursor: "pointer"}} /> }
          <div className="feedback__about">
            <div 
              className="feedback__username" 
              style={{cursor: "pointer"}} 
              onClick={props.onClick}>
                {props.info.lastname} {props.info.firstname} {props.info.secondname}, 
                {props.info.sex === 'male' ? 'М' : 'Ж'} 
                <span className="text-nowrap">{moment().diff(props.info.bday, 'years')} лет</span>
            </div>
            <div className="feedback__postdate">Заявка от {moment(props.createdAt).format('DD.MM.YY')}</div>
            
            <div className="feedback__body d-none d-md-flex">
              <a href={`tel:${props.info.phone.replace(/\D/g,'')}`} className="feedback__phone">{ICONS.phone}{props.info.phone}</a>
              <a href={`mailto:${props.info.email}`} className="feedback__email">{ICONS.email}{props.info.email}</a>
            </div>
          </div>
        </div>
        <div className="feedback__body d-md-none">
          <a href={`tel:${props.info.phone.replace(/\D/g,'')}`} className="feedback__phone">{ICONS.phone}{props.info.phone}</a>
          <a href={`mailto:${props.info.email}`} className="feedback__email">{ICONS.email}{props.info.email}</a>
        </div>
      </div>
      { props.full && 
        <div className="feedback__full">
          <h3>Информация о кандидате</h3>
          <Row className="align-items-center">
            <Col sm={4}>
              <label>Дата рождения</label>
            </Col>
            <Col>{moment(props.info.bday).format('DD.MM.YYYY')}</Col>
          </Row>
          <Row className="align-items-center">
            <Col sm={4}>
              <label>Место рождения</label>
            </Col>
            <Col>{props.info.bplace}</Col>
          </Row>
          <Row className="align-items-center">
            <Col sm={4}>
              <label>Серия и номер паспорта</label>
            </Col>
            <Col>{props.info.passportnum}</Col>
          </Row>
          <Row className="align-items-center">
            <Col sm={4}>
              <label>Регистрация</label>
            </Col>
            <Col>
              {props.info.regcity}, 
              {props.info.regstreet}, 
              д.{props.info.regbuild}{props.info.regcorps && `к${props.info.regcorps}`}
              {props.info.regapartment && `, кв.${props.info.regapartment}`}
            </Col>
          </Row>
          {props.info.actualplace !== 'true' && <Row className="align-items-center">
            <Col sm={4}>
              <label>Фактическое место проживания</label>
            </Col>
            <Col>
              {props.info.actualcity}, 
              {props.info.actualstreet}, 
              д.{props.info.actualbuild}{props.info.actualcorps && `к${props.info.actualcorps}`}
              {props.info.actualapartment && `, кв.${props.info.actualapartment}`}
            </Col>
          </Row>}
          <Row className="align-items-center">
            <Col sm={4}>
              <label>Фото/скан паспорта</label>
            </Col>
            <Col><RenderImg data={props.files.passport}/></Col>
          </Row>
          {props.files.employment.length > 0 && <Row className="align-items-center">
            <Col sm={4}>
              <label>Фото/скан трудовой книжки</label>
            </Col>
            <Col><RenderImg data={props.files.employment}/></Col>
          </Row>}
          <Row className="align-items-center">
            <Col sm={4}>
              <label>ИНН</label>
            </Col>
            <Col className="d-flex flex-wrap align-items-center">{props.info.inn}</Col>
            <Col sm={{offset : 4, size : 8}}><RenderImg data={props.files.inn}/></Col>
          </Row>
          <Row className="align-items-center">
            <Col sm={4}>
              <label>СНИЛС</label>
            </Col>
            <Col className="d-flex flex-wrap align-items-center">{props.info.snils}</Col>
            <Col sm={{offset : 4, size : 8}}><RenderImg data={props.files.snils}/></Col>
          </Row>
          { props.files.military.length > 0 && <Row className="align-items-center">
            <Col sm={4}>
              <label>Фото/скан военного билета</label>
            </Col>
            <Col><RenderImg data={props.files.military}/></Col>
          </Row>}

          { props.info.places && <Row>
            <Col sm={4}>
              <label>Последние места работы</label>
            </Col>
            <Col><RenderPlaces data={JSON.parse(props.info.places)} /></Col>
          </Row>} 

          <Row>
            <Col sm={{offset : 4, size : 8}}>
              <Row className="feedback__buttons">
                <Button color="link" onClick={() => console.log('Прощай жестокий мир!!!') }>Отказать кандидату</Button>
                <Flatpickr 
                  options={{
                    locale : Russian,
                    altInput: true,
                    // static : true,
                    minDate : new Date(),
                    enableTime : true,
                    time_24hr : true,
                    plugins: [new confirmDatePlugin({
                      // confirmText: "Ok ",
                      theme : "kari",
                    })],
                    wrap: true,
                  }}
                  className="feedback__calendar"
                  >
                  <input type='hidden' data-input />
                  <Button color="success" data-toggle onClick={() => console.log('Hello World!!!') }>Пригласить на собеседование</Button>
                </Flatpickr>
              </Row>
            </Col>
          </Row>

        </div> 
      }
    </React.Fragment>
  )
}

FeedbackItem.propTypes = {
  info: PropTypes.object.isRequired,
  files: PropTypes.object,
  full: PropTypes.bool
}

const RenderImg = (props) => {
  if ( typeof props.data === 'object'  ) {
    return ( 
      <ul className="dropzone__list">
        { 
          props.data.map((file, i) => {
            return (
              <li key={i} className="dropzone__item">
                <div className="dropzone__img">
                  <a href={`${server.url}/${file}`} target="_blank" rel="noopener noreferrer">
                    <img src={`${server.url}/${file}`} alt={file} />
                  </a>
                </div>
              </li>
            )
          }) 
        }
      </ul>
    )
  }
}

const RenderPlaces = props => {
  if ( typeof props.data === 'object' ){
    return (
      props.data.map((item, i) => {
        return (
          <div key={i} className="feedback__place">
            <div className="feedback-place"><strong>{item.name}</strong> <span className="text-nowrap">({moment(item.worktime[0]).format('MMMM YYYY')} &mdash; {moment(item.worktime[1]).format('MMMM YYYY')})</span></div>
            <div className="feedback-position"><strong>{item.position}</strong></div>
            <div className="feedback-func">{item.func}</div>
          </div>
        )
      })
    )
  }
}