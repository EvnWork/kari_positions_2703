import * as React from 'react'
import axios from 'axios';
import server from '../../serverConfig';
import Preload from '../../sugarComponents/Preloader'
import {FeedbackItem} from './Feedbacks'
import userpic from '../../assets/images/default-userpic.png'

export default class FeedbackInner extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      feedbackInfo : null
    }
  }

  async componentDidMount(){
    const authUser = JSON.parse(localStorage.getItem('authUser'))
    const feedbackInfo = await axios.get(
      `${server.url}/api/v1/questionnaire/${this.props.match.params.id}`, 
      {
        headers: { 'Authorization' : `Bearer ${authUser.token}` }
      }
    )
    this.setState({
      feedbackInfo : feedbackInfo.data
    })

    console.log(this.state.feedbackInfo)
  }

  render(){
    const { feedbackInfo } = this.state;
    if ( feedbackInfo ) {
      console.log(feedbackInfo.info.places);
      return(
        <React.Fragment>
          <div className="feedback-full feedback-list">
            <FeedbackItem 
              key={feedbackInfo.id}

              info={{...feedbackInfo.info}}
              files={{...feedbackInfo.files}}
              full={true}
              />
          </div>
        </React.Fragment>
      )
    } else {
      return <React.Fragment><Preload /></React.Fragment>
    }
  }
}