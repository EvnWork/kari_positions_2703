import * as React from "react";
import axios from "axios";
import moment from 'moment'
import server from '../serverConfig'
import 'moment/locale/ru';

// moment.locale('ru')

// interface ILKProps {
//   prop?: any
// }
// interface ILKState {
//   userData?: object[]
// }

const RenderImg = (props) => {
  if ( typeof props.data === 'object'  ) {
    return ( 
      <ul className="dropzone__list">
        { 
          props.data.map((file, i) => {
            return (
              <li key={i} className="dropzone__item">
                <div className="dropzone__img">
                  <a href={`${server.url}/${file}`} target="_blank" rel="noopener noreferrer">
                    <img src={`${server.url}/${file}`} alt={file} />
                  </a>
                </div>
              </li>
            )
          }) 
        }
      </ul>
    )
  }
}

const RenderPlaces = props => {
  if ( typeof props.data === 'object' ){
    return (
      props.data.map((item, i) => {
        return (
          <div key={i}>
            <div>{item.name} ({moment(item.worktime[0]).format('MMMM YYYY')} &mdash; {moment(item.worktime[1]).format('MMMM YYYY')})</div>
            <div>{item.position}</div>
            <div>{item.func}</div>
          </div>
        )
      })
    )
  }
}

const getCityName = async cityId => {
  if ( cityId ) {
    const cityName = await axios.get(
      `https://kari-dev.lad24.ru/cloud/dictionaries/city?cityId=${cityId}`,
      {
        headers: {
          'Authorization': `${server.lcfToken}`,
        }
      }
    );

    return cityName
  }
}

export default class LK extends React.Component{

  constructor(props){
    super(props)
    this.state = {
      userData : null,
      cities : null,
    }
  }

  async componentDidMount(){
    const userData = await axios.get(
      `${server.url}/api/v1/questionnaire`, 
      {
        headers: { 'Authorization' : `${server.token}` }
      }
    )
    this.setState({
      userData : userData.data
    })

    const cities = []
    const citiesArr = []
    this.state.userData.map((item, i) => {
      // console.log(item);
      cities.push(item.info.city);
    })
    
    for (let i = 0; i < cities.length; i++) {
      const cityName = await axios.get(
        `https://kari-dev.lad24.ru/cloud/dictionaries/city?cityId=${cities[i]}`,
        {
          headers: {
            'Authorization': `${server.lcfToken}`,
          }
        }
      );
      // console.log(cityName);
      citiesArr.push(cityName.data.cities[0]);
    }

    // console.log(citiesArr);
  }

  render(){

    const data = this.state.userData;
    // console.log('123', data);

    if ( data !== null ) { 

      return data.map( (item, i) => {

        const info = item.info;
        const files = item.files;
        let sex
        if ( info.sex === 'male' ) {
          sex = "М"
        } else {
          sex = "Ж"
        }

        let workCity = getCityName(info.city)
        // getCityName(info.city).then(
        //   result => {
        //     console.log("Fulfilled: ", result);
        //     workCity = result
        //   },
        //   error => {
        //     console.log("Rejected: ", error);
        //   }
        // )
        
        // console.log(123, workCity);

        let age = moment().diff(info.bday, 'years')

        let phoneClear = info.phone !== undefined ? info.phone.replace(/\D/g,'') : ''
        
        let reg = info.regcity + ", " + info.regstreet + ", д." + info.regbuild
        if (info.regcorps) {
          reg += ", к." + info.regcorps
        }
        if (info.regapartment) {
          reg += ", кв." + info.regapartment
        }

        let actual
        if ( info.actualplace !== true ) {
          actual = "Фактическое место проживания: " + info.actualcity + ", " + info.actualstreet + ", д." + info.actualbuild
          if (info.actualcorps) {
            actual += ", к." + info.actualcorps
          }
          if (info.actualapartment) {
            actual += ", кв." + info.actualapartment
          }
        }

        let lastPlaces = ( info.places ) ? JSON.parse(info.places) : null;

        let bday = ( typeof info.bday === "object" ) ? info.bday[0] : info.bday
      
        return(
          <div key={item.id} className="mb-5 p-3">
            <h3>{ `${info.lastname} ${info.firstname} ${info.secondname}, ${sex} ${age} лет` }</h3>
            <div>Заявка от {moment(item.createdAt).format('DD.MM.YY')}</div>
            <div>Номер телефона: <a href={'tel:+' + phoneClear}>{info.phone}</a></div>
            <div>E-mail: <a href={'mailto:' + info.email}>{info.email}</a></div>

            <h3>Информация о кандидате</h3>
            <div>Дата рождения {moment(bday).format('DD.MM.YYYY')}</div>
            <div>{ `Место рождения: ${info.bplace}`}</div>
            <div>{ `Серия и номер паспорта: ${info.passportnum}` }</div>
            <div>{ `Регистрация: ${reg}` }</div>
            {( info.actualplace !== "true" ) ? <div>{ actual }</div> : "" }
            { 
              ( files.passport && files.passport.length > 0 ) 
              ? <div>Фото/скан паспорта <RenderImg data={files.passport} /></div> 
              : '' 
            }
            { 
              ( files.employment && files.employment.length > 0 ) 
              ? <div>Фото/скан трудовой книжки <RenderImg data={files.employment} /></div> 
              : '' 
            }
            <div>
            { `ИНН: ${info.inn}` }
            {
              ( files.inn && files.inn.length > 0 ) 
              ? <RenderImg data={files.inn} />
              : ''
            }
            </div>
            <div>
              { `СНИЛС: ${info.snils}` }
              {
                ( files.snils && files.snils.length > 0 ) 
                ? <RenderImg data={files.snils} /> 
                : ''
              }
            </div>

            <div>
              {
                ( lastPlaces !== null ) ?
                <div>Последние места работы <RenderPlaces data={lastPlaces} /></div>
                : ''
              }
            </div>
          </div>
        )
      })
    } else {
      return (
        <div>Loading...</div>
      )
    }
  }
}

