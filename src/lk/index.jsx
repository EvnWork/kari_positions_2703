import * as React from 'react'
// import axios from "axios";
// import moment from 'moment'
import purpurBurger from "../assets/images/icons/burge_purpur.png";
import { 
  // BrowserRouter as Router,
  Link, 
  NavLink as RouteLink, 
  // Redirect,
  Route, 
  // withRouter, 
} from "react-router-dom"

import Feedbacks from './components/Feedbacks'
import FeedbackInner from './components/FeedbackInner';
import PositionsWindow from './positions/PositionsWindow';

import logo from '../assets/images/logo.svg'
import user from '../assets/images/icons/user.svg'
import { 
  Button, 
  ButtonDropdown, 
  Col, 
  Container, 
  DropdownItem, 
  DropdownMenu, 
  DropdownToggle, 
  Nav, 
  NavItem, 
  NavLink, 
  Row, 
} from 'reactstrap'
import Preload from '../sugarComponents/Preloader'

const MENU_ICON = <svg width="29" height="16" viewBox="0 0 29 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M0 16H29V14H0V16ZM0 2H29V0H0V2ZM0 9H29V7H0V9Z" fill="black"/></svg>
const CLOSE_ICON = <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M11.446 10L20 18.554L18.553 20L10 11.446L1.446 20L0 18.554L8.553 10L0 1.446L1.446 0L10 8.554L18.553 0L20 1.446L11.446 10Z" fill="#7F8184"/></svg>

export default class LK extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      menuOpen : false,
      openDropdown : false,
      /* evg: чуть ниже метод жизненного цикла извлекает какие-то данные из локал стораджа
        и добавляет в юзердату, у меня тут все естественноы ломалось, поэтому я отключил*/
      // userData : null   TODO:  раскоментить
        userData: {        // TODO: убрать
        surname: 'Porosenok',
            name: 'Petr'
        }
    }
    this.menuOpenHandler = this.menuOpenHandler.bind(this)
    this.toggleDropdown = this.toggleDropdown.bind(this)
  }

  async componentDidMount() {
    // const lStorage = JSON.parse(localStorage.getItem('authUser')) TODO:  раскоментить
    // const jwtToken = lStorage.token.split('.')
    // const decodedData = Buffer.from(jwtToken[1], 'base64').toString('utf-8')
    // // console.log(JSON.parse(decodedData))
    // this.setState({
    //   userData : JSON.parse(decodedData)
    // })
  }

  menuOpenHandler(){
    this.setState(prevState => ({
      menuOpen : !prevState.menuOpen
    }))
  }

  toggleDropdown = () => {
    this.setState(prevState => ({
      openDropdown : !prevState.openDropdown
    }))
  }

  exitClick = () => {
    localStorage.removeItem('authUser')
    console.log(localStorage.getItem('authUser'))
    this.props.history.push('/login')
  }

  render(){
    const { userData, openDropdown, menuOpen } = this.state;
    if (this.state.userData) {
      return (
        <Container fluid style={{minHeight: "100%"}} className="d-flex flex-column container-js">
          <header className="header-lk">
            <Row>
              <Col lg={4} xl={3}
                   /*evg: друзья, у меня в макете другой хедер, не знаю как вы в будущем собираетесь управлять его видом, через
                   глобальный стор или внутренний стейт, но пока я сделал вот так, чтобы соответствовать макету*/
                   className={`header-lk__logo ${ window.location.pathname.includes('/protected/test') ? 'light' : ''}`}
              >
                <Link to="/protected" exact="true">
                  {console.log(window.location)}
                  {
                    // evg: здесь тоже меняю хедер
                    window.location.pathname.includes('/protected/test') && window.innerWidth<991 ?
                        false :  (<img src={logo} alt="logo" />)
                  }
                  {
                    window.location.pathname.includes('/protected/test')  && window.innerWidth<991 ?
                         (<span>Должности</span>) : ( <span>Личный кабинет <br/> директора</span>)
                  }

                </Link>
                <Button 
                  onClick={this.menuOpenHandler} 
                  className="header-lk__btn d-lg-none d-xlg-none ml-auto positionStyle">
                  { window.location.pathname.includes('/protected/test')  && window.innerWidth<991 ?
                      <img src={purpurBurger} style={{height:'16px', width: '24px'}}/>
                      : MENU_ICON }
                </Button>
              </Col>
                  {/* evg: Лера говорит шапка на десктопе не соответствует. Не знаю что тут с ней сделать ( с шапкой), просто покрасил   */}
              <Col className="d-none d-lg-flex d-xl-flex" style={{borderBottom: '2px solid #fff',  backgroundColor:' #991E66'}} >
                {/* <ProfileHeader 
                  key="1"

                  username={`${userData.surname} ${userData.name}`}
                  userposition={userData.positionName}
                  className="ml-auto" 
                  exitClick={ () => this.exitClick() }
                  openDropdown={openDropdown} 
                  toggleDropdown={this.toggleDropdown} /> */}
              </Col>
            </Row>
          </header>
          <Row style={{flexGrow: 1}}>
            <Col lg={4} xl={3} className={`aside ${menuOpen ? 'active' : ''}`} >
              <div className="header-lk__logo header-lk__logo_transparent d-lg-none d-xlg-none">
                <Link to="/protected" exact="true">
                  <img src={logo} alt="logo" />
                  <span>Личный кабинет <br/> директора</span>
                </Link>
                <Button 
                  onClick={this.menuOpenHandler} 
                  className="header-lk__btn d-lg-none d-xlg-none ml-auto">
                  {CLOSE_ICON}
                </Button>
              </div>
              <RenderNav className="aside__nav" data={navList} />
              <ProfileHeader
                key="2" 
                username={`${userData.surname} ${userData.name}`}
                userposition={userData.positionName}
                openDropdown={openDropdown} 
                toggleDropdown={this.toggleDropdown}
                exitClick={ () => this.exitClick() }
                className="d-lg-none d-xlg-none mt-auto" />
            </Col>
            {/* Evg: т.к. ваш основной компонент смещает паддингом страницу,
            которую я делаю, на 15px в обе стороны, не давая соблюдать единый цвет
             фона по макету, устанавливаю цвет в этой обертке до работы паддинга
             (потом переместите куда вам надо) */}
            <Col style={{backgroundColor: '#F3F6FA'}}>
              <Route exact={true} path="/protected" component={Test1} />
              <Route exact={true} path="/protected/test" component={PositionsWindow} />
              <Route exact={true} path="/protected/feedback" component={Feedbacks} />
              <Route exact={true} path="/protected/test2" component={Test2} />
              <Route
                  exact={true}
                  path="/protected/feedback/:id"
                  component={FeedbackInner} 
                  />
            </Col>
          </Row>
          { menuOpen && <div onClick={this.menuOpenHandler} className="menu-shadow"></div>}
        </Container>
      )
    } else {
      return <Preload className="fixed" />
    }
  }
}

const ProfileHeader = props => {

  return(
    <React.Fragment>
      <ButtonDropdown isOpen={props.openDropdown} toggle={props.toggleDropdown} className={props.className}>
        <DropdownToggle className={`header-lk__user`}>
          <div className="userpic">
            <img src={ props.userpic ? props.userpic : user } alt=""/>
          </div>
          <div className="usertext">
            <div className="username">{ props.username ? props.username : 'Александр Александров' }</div>
            <div className="userposition">{ props.userposition ? props.userposition : 'директор магазина' }</div>
          </div>
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem onClick={ props.exitClick }>Выход</DropdownItem>
        </DropdownMenu>
      </ButtonDropdown>
    </React.Fragment>
  )
}

const RenderNav = props => {
  const { data } = props
  return (
    <Nav vertical className={props.className}>
      {data.map((item, i) => {
        return (
          <NavItem key={`RenderNav_${i}`}>
            <NavLink tag={RouteLink} to={item.link} activeClassName='active'>
              {item.icon} <span>{item.text}</span>
            </NavLink>
          </NavItem>
        )
      })}
    </Nav>
  )
}

const Test = props => {
  return (
    <div>Test 1</div>
  )
}

const Test1 = props => {
  return (
    <div>Главная страница Личного кабинета</div>
  )
}

const Test2 = props => {
  return (
    <div>Test 3</div>
  )
}

const navList = [
  {
    icon : <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M10 9.99998C7.7835 9.99998 5.98095 8.20598 5.98095 5.99998C5.98095 3.79398 7.7835 1.99998 10 1.99998C12.2165 1.99998 14.019 3.79398 14.019 5.99998C14.019 8.20598 12.2165 9.99998 10 9.99998ZM13.7759 10.673C15.3704 9.39598 16.2999 7.33098 15.9582 5.06998C15.5614 2.44698 13.369 0.347977 10.7224 0.041977C7.07012 -0.381023 3.97143 2.44898 3.97143 5.99998C3.97143 7.88998 4.8516 9.57398 6.22411 10.673C2.85213 11.934 0.390463 14.895 0.0046342 18.891C-0.0516324 19.482 0.411563 20 1.00839 20C1.51981 20 1.95588 19.616 2.0011 19.109C2.404 14.646 5.83727 12 10 12C14.1627 12 17.596 14.646 17.9989 19.109C18.0441 19.616 18.4802 20 18.9916 20C19.5884 20 20.0516 19.482 19.9954 18.891C19.6095 14.895 17.1479 11.934 13.7759 10.673Z" fill="#1F1F1F"/></svg>,
    link : '/protected/test',
    text : 'Должности / Cотрудники'
  },
  {
    icon : <svg width="16" height="20" viewBox="0 0 16 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M12 14H4V8C4 5.334 6 4 7.999 4H8.001C10 4 12 5.334 12 8V14ZM9 17C9 17.552 8.552 18 8 18C7.448 18 7 17.552 7 17V16H9V17ZM14 14V8C14 4.447 11.632 2.475 9 2.079V0H7V2.079C4.368 2.475 2 4.447 2 8V14H0V16H5V17C5 18.657 6.343 20 8 20C9.657 20 11 18.657 11 17V16H16V14H14Z" fill="#D8DAE0"/></svg>,
    link : '/protected/feedback',
    text : 'Отклики на вакансии'
  },
  {
    icon : <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M17.9712 8H1.99681V4.971C1.99681 4.435 2.43111 4 2.96625 4H3.99361V6H5.99042V4H13.9776V6H15.9744V4H16.9728C17.524 4 17.9712 4.448 17.9712 5V8ZM17.9712 17C17.9712 17.55 17.522 18 16.9728 18H2.99521C2.44409 18 1.99681 17.552 1.99681 17V10H17.9712V17ZM0.0638978 17.761C0.0638978 18.865 1.02137 20 2.1236 20H18.098C19.2013 20 20 18.979 20 17.761C20 17.372 19.9681 5.36 19.9681 4.708C19.9681 2.626 19.6875 2 15.9744 2V0H13.9776V2H5.99042V0H3.99361V2H1.99681C0.898562 2 0 2.9 0 4L0.0638978 17.761Z" fill="#D8DAE0"/></svg>,
    link : '/protected/test2',
    text : 'Календарь встреч'
  },
]