import * as React from 'react'
import logo from '../assets/images/logo.svg'

const Preload = props => {
  return (
    <div className={`preloader d-flex align-items-center justify-content-center ${props.className}`}>
      <img src={logo} alt="preloader" style={style}/>
    </div>
  )
}
const style={
  animation: 'square-spin 3s 0s cubic-bezier(.09,.57,.49,.9) infinite'
}

export default Preload