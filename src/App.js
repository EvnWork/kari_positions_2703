import * as React from "react";
import { 
  BrowserRouter as Router,
  Redirect,
  Route, 
  // withRouter,
} from "react-router-dom";
import { 
  Button, 
  Input,
  Col, 
  Container,
  FormGroup,
  Label, 
  Row,
} from 'reactstrap';
import Profile from "./profile/index";
import LK from "./lk/index";
import MainPage from "./mainPage/index"
import Axios from "axios";
import server from "./serverConfig";
import Header from './profile/components/Header'

const fakeAuth = {
  isAuthenticated: true,
  authenticate(cb) {
    this.isAuthenticated = true;
    setTimeout(cb, 100); // fake async
  },
  signout(cb) {
    this.isAuthenticated = false;
    setTimeout(cb, 100);
  }
};

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render = {
        props => 
          fakeAuth.isAuthenticated 
          ? ( <Component {...props} /> ) 
          : ( <Redirect to={{
                pathname: "/login",
                state: { from: props.location }
              }}
            /> )
      }
    />
  );
}

export default class App extends React.Component{
  render() {
    return(
      <Router>
        <React.Fragment>
          <Route exact={true} path="/" component={MainPage} />
          <Route path="/profile" component={Profile} />
          <Route path="/login" component={Login} />
          <PrivateRoute path="/protected" component={LK} />
        </React.Fragment>
      </Router>
    )
  }
}

class Login extends React.Component {
  state = { 
    // redirectToReferrer: false,
    redirectToReferrer: fakeAuth.isAuthenticated,
    errorSend : false,
    errorMsg : null
  };

  async componentDidMount(){
    let lStorage = JSON.parse(localStorage.getItem('authUser'));

    //try {
      // let login = await Axios.get(
      //   `${server.url}/api/v1/questionnaire`,
      //   {
      //     headers: {
      //       'Authorization': `Bearer ${lStorage.token}`,
      //     }
      //   }
      // )
    //} catch (error) {
    let login = lStorage ? lStorage.expiresIn * 1000 > new Date().getTime() : null
    if (login) {
      console.log('login? ---> ', login);
      fakeAuth.authenticate(() => {
        this.setState({ 
          redirectToReferrer: true,
        })
      })
    } else {
      console.log('error login ---> ');

      try {
        console.log(lStorage.refreshToken)
        let newLogin = await Axios.get(
          `${server.url}/api/v1/refresh`,
          {
            headers: {
              'Authorization': `Bearer ${lStorage.refreshToken}`,
              'Content-Type' : 'application/json'
            }
          }
        )
        localStorage.setItem('authUser', JSON.stringify(newLogin.data))
        newLogin && fakeAuth.authenticate(() => {
          this.setState({ 
            redirectToReferrer: true,
          })
        })
        console.log('refresh ? --->', newLogin)
      } catch (er) {
        console.log('error refresh --->', er.response);
        localStorage.removeItem('authUser');
      }
    }

  }

  login = async(e) => {
    e.preventDefault()

    try {
      let login = await Axios.post(
        `${server.url}/api/v1/login`, 
        {
          login : this.state.login,
          password : this.state.password
        }
      )

      console.log(login);

      if ( login.status === 200 ) {
        localStorage.setItem('authUser', JSON.stringify(login.data));
        fakeAuth.authenticate(() => {
          this.setState({ 
            redirectToReferrer: true, 
            errorSend : false,
            errorMsg : null
          })
        })
      }
    } catch(error) {
      console.log(error.response);
      const errMsg = error.response.data.message;
      this.setState({
        errorMsg : errMsg,
        errorSend : true
      })
    }
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    let { from } = this.props.location.state || { from: { pathname: "/protected/feedback" } };
    let { redirectToReferrer } = this.state;

    if (redirectToReferrer) return <Redirect to={from} />;

    return (
      <React.Fragment>
        <Header />
        <Container>
          <Row>
            <Col sm="12" lg={{ size: 10, offset: 1 }} className="form-wrapper">
              <form onSubmit={this.login} className={`profileForm`} >
                <FormGroup row={true}>
                  <Label 
                    for="profileForm__login" 
                    sm={5} 
                    className="profileForm__label">Логин</Label>
                  <Col sm={7}>
                    <Input 
                      type="text" 
                      name="login" 
                      className={`${ this.state.errorSend ? 'error' : ''}`}
                      id="profileForm__login" 
                      onChange={this.handleInputChange}
                      />
                  </Col>
                </FormGroup>
                <FormGroup row={true}>
                  <Label 
                    for="profileForm__password" 
                    sm={5} 
                    className="profileForm__label">Пароль</Label>
                  <Col sm={7}>
                    <Input 
                      type="password" 
                      name="password" 
                      className={`${ this.state.errorSend ? 'error' : ''}`}
                      id="profileForm__password"
                      onChange={this.handleInputChange}
                      />
                  </Col>
                </FormGroup>
                { this.state.errorMsg && <FormGroup className="text-center">
                  <p className="text-danger">{this.state.errorMsg}</p>
                </FormGroup> }
                <FormGroup className="text-center">
                  <Button color="primary" className="btn_brand btn-xlg" type="submit">Вход</Button>
                </FormGroup>
              </form>
            </Col>
          </Row>
        </Container>
      </React.Fragment>
    );
  }
}
