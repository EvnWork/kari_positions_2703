import * as React from 'react'
import { Container, Button } from 'reactstrap'
import { Link } from "react-router-dom";

import logo from '../assets/images/logo.svg';
import people from '../assets/images/people.png';

const MainPage = (props) => {
  return (
    <header className="header main">
      <Container className="position-relative">
        <img src={logo} className="header__logo" alt="logo" />
        <h1 className="header__title">Работай, когда удобно — зарабатывай, сколько хочешь в компании №1 в России</h1>
        <Button size="lg" color="primary" tag={Link} to="/profile">Выбрать вакансию</Button>
      </Container>
      <img src={people} className="header__people" alt="people" />

    </header>
  )
}

export default MainPage
