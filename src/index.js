import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import './assets/scss/index.scss';
// import serviceWorker from './serviceWorker';

import {Provider} from 'react-redux';
import {createStore, combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
const reducers = {form: formReducer};
const reducer = combineReducers(reducers);
const store = createStore(reducer);

ReactDOM.render(
  <Provider store={store}><App /></Provider>, 
  document.getElementById('root')
);
// serviceWorker();
