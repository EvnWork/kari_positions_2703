import * as React from 'react'
import Header from './components/Header'
// import ProfileFormComponent from './ProfileForm'
import axios from 'axios'
import { Modal, ModalBody, ModalFooter, ModalHeader, Button } from 'reactstrap'
import { reset } from 'redux-form'
import server from '../serverConfig'

import ProfileFormComponent from './components/form/profileMain'
import MarketSelect from './components/form/profileMarketSelect'
import PhoneValidate from './components/form/profilePhone'

import Preload from '../sugarComponents/Preloader'

import './components/_profileForm.scss'

const postEvents = async (data) => {
  try {
    return await axios.post(
      `${server.url}/api/v1/questionnaire`,
      data,
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    );
  } catch (error) {
    // console.log("Bad postEvents: ", error);
    return error
  }
}

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      thx: false,
      page: 1,
      sendData : false,
      modalText : {
        header : null,
        body : null
      }
    }

    this.toggle = this.toggle.bind(this);
    // this.thxToggle = this.thxToggle.bind(this);
    this.nextPage = this.nextPage.bind(this)
    this.previousPage = this.previousPage.bind(this)
  }
  nextPage() {
    this.setState({ page: this.state.page + 1 })
  }

  previousPage() {
    this.setState({ page: this.state.page - 1 })
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  thxToggle = (status = 200, header = 'Спасибо', body = 'Спасибо за заявку. Она будет обработанна в ближайшее время.') => {
    this.setState({
      modalText : {
        header : header,
        body : body
      }
    })

    if (status === 200){
      if ( this.state.thx === false ) {
        this.setState(prevState => ({
          thx: !prevState.thx
        }));
      } else {
        this.setState(prevState => ({
          thx: !prevState.thx
        }));
        setTimeout(() => { this.props.history.push("/") }, 750);
      }
    } else {
      this.setState(prevState => ({
        thx: !prevState.thx
      }));
    }
  }

  submit = async (data, dispatch) => {

    let body = new FormData();
    Object.keys(data).forEach((key) => {
      if (key === 'files') {
        for (let files in data[key]) {
          for (let i = 0; i < data[key][files].length; i++) {
            body.append(`${key}[${files}]`, data[key][files][i], data[key][files][i].name)
          }
        }
      } else if (key === 'places') {
        body.append( key, JSON.stringify(data[key]) );
      } else {
        body.append(key, data[key]);
      }
    });

    this.setState(prevState => ({
      sendData : !prevState.sendData
    }))

    const sendData = await postEvents(body);
    if (sendData.status === 200){
      this.setState(prevState => ({
        sendData : !prevState.sendData
      }))
      dispatch(reset("profile"))
      this.thxToggle();
    } else {
      this.setState(prevState => ({
        sendData : !prevState.sendData
      }))
      this.thxToggle(400, 'Ошибка', 'При выполнении операции произошла ошибка! Приносим свои извинения за доставленные неудобства. Информация об ошибке передана в службу технической поддержки. Попробуйте повторить операцию позже.')
    }
  };



  render() {
    // const { onSubmit } = this.props
    const { page } = this.state
    return (

      <div className="App">
        <Header />
        
        <React.Fragment>
          {page === 1 && 
            <MarketSelect 
              onSubmit={this.nextPage}/>}
          {page === 2 && 
            <PhoneValidate 
              previousPage={this.previousPage} 
              onSubmit={this.nextPage}/>}
          {page === 3 && 
            <ProfileFormComponent 
              previousPage={this.previousPage} 
              onSubmit={this.submit} 
              modal={this.state.modal} 
              toggle={this.toggle} />}
        </React.Fragment>

        <Modal isOpen={this.state.thx} toggle={() => this.thxToggle()}>
          <ModalHeader toggle={this.thxToggle}>{this.state.modalText.header}</ModalHeader>
          <ModalBody>
            {this.state.modalText.body}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.thxToggle()}>Закрыть</Button>
          </ModalFooter>
        </Modal>

        { this.state.sendData && <Preload className="fixed" /> }
      </div>
    )
  }
}

export default Profile;
