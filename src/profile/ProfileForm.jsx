import * as React from 'react';
import { 
  Button, 
  Col, 
  Container, 
  Form, 
  FormGroup,
  Label, 
  Row,
} from 'reactstrap';
import './_profileForm.scss';
import {
  renderPlaces,
  renderField, 
  renderCheckbox, 
  renderDatePicker,
  renderUpload
} from './components/formComponents';
import { connect } from 'react-redux'
import {
  Field, 
  FieldArray,
  formValueSelector, 
  reduxForm, 
  reset,
} from 'redux-form';

import IMask from 'imask';
import { required, email, minNum } from './components/form/validation'
import ModalPolitic from './components/form/modalPolitic'

import ProfileFormComponent from './components/form/profileMain'
import MarketSelect from './components/form/profileMarketSelect'

/*let ProfileFormComponent = (props) => {
  const {
    actualPlace,
    employmentHistory,
    handleSubmit, 
    isRussian, 
    militaryLiable,
    submitting, 
  } = props;

  setIMask('.form-inn', maskList.inn);
  setIMask('.form-passport', maskList.passport);
  setIMask('.form-phone', maskList.phone);
  setIMask('.form-snils', maskList.snils);

  return(
    <Container>
      <Row>
        <Col className="form-wrapper" sm="12" lg={{ size: 10, offset: 1 }}>
          <Form 
            model="user"
            encType="multipart/form-data"
            className="profileForm" 
            onSubmit={handleSubmit}>
            <legend className="profileForm__title text-center">Давайте знакомиться :)</legend>
            <FormGroup row={true}>
              <Col sm={5}>
                <legend className="profileForm__label profileForm__label_medium">
                  Вы гражданин России?
                </legend>
              </Col>
              <Col sm={7}>
                <div className="form-check-inline h-100">
                  <Field
                    component={renderCheckbox}
                    className="profileForm__radio profileForm__radio_inline"
                    name="citizenship"
                    type="radio"
                    value="true"
                    validate={[required]}
                    >
                      Да
                    </Field>
                  <Field
                    component={renderCheckbox}
                    className="profileForm__radio profileForm__radio_inline"
                    name="citizenship"
                    type="radio"
                    value="false"
                    validate={[required]}
                    >
                      Нет
                    </Field>
                </div>
              </Col>
            </FormGroup>

            {isRussian==="true" && <div>

            <FormGroup row={true}>
              <Label 
                for="profileForm__market" 
                sm={5} 
                className="profileForm__label">Адрес магазина</Label>
              <Col sm={7}>
                <Field 
                  className="form-control" 
                  component={renderField} 
                  type="text" 
                  name="market" 
                  placeholder="ТЦ Жарптица"
                  validate={[required]}
                  id="profileForm__market" />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label for="profileForm__lastname" sm={5} className="profileForm__label">Фамилия</Label>
              <Col sm={7}>
                <Field 
                  className="form-control" 
                  component={renderField} 
                  type="text" 
                  name="lastname" 
                  validate={[required]}
                  id="profileForm__lastname" />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label for="profileForm__firstname" sm={5} className="profileForm__label">Имя</Label>
              <Col sm={7}>
                <Field 
                  className="form-control" 
                  component={renderField} 
                  type="text" 
                  name="firstname" 
                  validate={[required]}
                  id="profileForm__firstname" />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label for="profileForm__secondname" sm={5} className="profileForm__label">Отчество</Label>
              <Col sm={7}>
                <Field 
                  className="form-control" 
                  component={renderField} 
                  type="text" 
                  name="secondname" 
                  validate={[required]}
                  id="profileForm__secondname" />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Col sm={5}>
                <legend className="profileForm__label profileForm__label">
                  Пол
                </legend>
              </Col>
              <Col sm={7}>
                <div className="form-check-inline h-100">
                  <Field
                    component={renderCheckbox}
                    className="profileForm__radio profileForm__radio_inline"
                    name="sex"
                    type="radio"
                    value="male"
                    validate={[required]}
                    >
                      Мужской
                    </Field>
                  <Field
                    component={renderCheckbox}
                    className="profileForm__radio profileForm__radio_inline"
                    name="sex"
                    type="radio"
                    value="female"
                    validate={[required]}
                    >
                      Женский
                    </Field>
                </div>
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label for="profileForm__passportnum" sm={5} className="profileForm__label">Серия и номер паспорта</Label>
              <Col sm={7}>
                <Field 
                  className="form-control form-passport" 
                  component={renderField} 
                  type="text" 
                  name="passportnum" 
                  validate={[required, minNum(10)]}
                  id="profileForm__passportnum" 
                  style={{maxWidth: '154px'}} />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label for="profileForm__bday" sm={5} className="profileForm__label">Дата рождения</Label>
              <Col sm={7}>
                <Field 
                  name="bday"
                  className="form-control" 
                  style={{maxWidth: '133px'}}
                  component={renderDatePicker} 
                  options={{
                    dateFormat : "d.m.Y",
                    minDate : '01.01.1901',
                    mode : 'single'
                  }}
                  validate={[required]} 
                  />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label 
                for="profileForm__bplace" 
                sm={5} 
                className="profileForm__label">
                Место рождения
              </Label>
              <Col sm={7}>
                <Field 
                  className="form-control" 
                  component={renderField} 
                  type="text" 
                  name="bplace" 
                  validate={[required]}
                  id="profileForm__bplace" />
              </Col>
            </FormGroup>
            <FormGroup>
              <legend className="profileForm__label profileForm__label_medium">Регистрация</legend>
            </FormGroup>
            <FormGroup row={true}>
              <Label 
                for="profileForm__regcity" 
                sm={5} 
                className="profileForm__label">
                Город
              </Label>
              <Col sm={7}>
                <Field 
                  className="form-control" 
                  component={renderField} 
                  type="text" 
                  name="regcity" 
                  validate={[required]}
                  id="profileForm__regcity" />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label 
                for="profileForm__regstreet" 
                sm={5} 
                className="profileForm__label">
                Улица
              </Label>
              <Col sm={7}>
                <Field 
                  className="form-control" 
                  component={renderField} 
                  type="text" 
                  name="regstreet" 
                  validate={[required]}
                  id="profileForm__regstreet" />
              </Col>
            </FormGroup>

            <Row>
              <Col xs={4} sm={12}>
                <FormGroup row={true}>
                  <Label 
                    for="profileForm__regbuild" 
                    sm={5} 
                    className="profileForm__label">
                    Дом
                  </Label>
                  <Col sm={7}>
                    <Field 
                      className="form-control" 
                      component={renderField} 
                      type="text" 
                      name="regbuild" 
                      validate={[required]}
                      id="profileForm__regbuild" 
                      style={{maxWidth: '133px'}} />
                  </Col>
                </FormGroup>
              </Col>
              <Col xs={4} sm={12}>
                <FormGroup row={true}>
                  <Label 
                    for="profileForm__regcorps" 
                    sm={5} 
                    className="profileForm__label">
                    Корпус
                  </Label>
                  <Col sm={7}>
                    <Field 
                      className="form-control" 
                      component={renderField} 
                      type="text" 
                      name="regcorps" 
                      id="profileForm__regcorps" 
                      style={{maxWidth: '133px'}} />
                  </Col>
                </FormGroup>
              </Col>
              <Col xs={4} sm={12}>
                <FormGroup row={true}>
                  <Label 
                    for="profileForm__regapartment" 
                    sm={5} 
                    className="profileForm__label">
                    Квартира
                  </Label>
                  <Col sm={7}>
                    <Field 
                      className="form-control" 
                      component={renderField} 
                      type="text" 
                      name="regapartment" 
                      id="profileForm__regapartment" 
                      style={{maxWidth: '133px'}} />
                  </Col>
                </FormGroup>
              </Col>
            </Row>
            
            <FormGroup row={true}>
              <Col sm={5}>
                <legend 
                  className="profileForm__label profileForm__label_medium">
                  Фактическое место проживания
                </legend>
              </Col>
              <Col sm={7}>
                <Field
                  component={renderCheckbox}
                  className="profileForm__checkbox"
                  name="actualplace"
                  type="checkbox"
                  value="true">
                    Совпадает с адресом регистрации
                  </Field>
              </Col>
            </FormGroup>
            
            { !actualPlace && <div>
              <FormGroup row={true}>
                <Label 
                  for="profileForm__actualcity" 
                  sm={5} 
                  className="profileForm__label">
                  Город
                </Label>
                <Col sm={7}>
                  <Field 
                    className="form-control" 
                    component={renderField} 
                    type="text" 
                    name="actualcity" 
                    id="profileForm__actualcity" />
                </Col>
              </FormGroup>

              <FormGroup row={true}>
                <Label 
                  for="profileForm__actualstreet" 
                  sm={5} 
                  className="profileForm__label">
                  Улица
                </Label>
                <Col sm={7}>
                  <Field 
                    className="form-control" 
                    component={renderField} 
                    type="text" 
                    name="actualstreet" 
                    id="profileForm__actualstreet" />
                </Col>
              </FormGroup>
              
              <Row>
                <Col xs={4} sm={12}>
                  <FormGroup row={true}>
                    <Label 
                      for="profileForm__actualbuild" 
                      sm={5} 
                      className="profileForm__label">
                      Дом
                    </Label>
                    <Col sm={7}>
                      <Field 
                        className="form-control" 
                        component={renderField} 
                        type="text" 
                        name="actualbuild" 
                        id="profileForm__actualbuild" 
                        style={{maxWidth: '133px'}} />
                    </Col>
                  </FormGroup>
                </Col>
                <Col xs={4} sm={12}>
                  <FormGroup row={true}>
                    <Label 
                      for="profileForm__actualcorps" 
                      sm={5} 
                      className="profileForm__label">
                      Корпус
                    </Label>
                    <Col sm={7}>
                      <Field 
                        className="form-control" 
                        component={renderField} 
                        type="text" 
                        name="actualcorps" 
                        id="profileForm__actualcorps" 
                        style={{maxWidth: '133px'}} 
                        />
                    </Col>
                  </FormGroup>
                </Col>
                <Col xs={4} sm={12}>
                  <FormGroup row={true}>
                    <Label 
                      for="profileForm__actualapartment" 
                      sm={5} 
                      className="profileForm__label">
                      Квартира
                    </Label>
                    <Col sm={7}>
                      <Field 
                        className="form-control" 
                        component={renderField} 
                        type="text" 
                        name="actualapartment" 
                        id="profileForm__actualapartment" 
                        style={{maxWidth: '133px'}} 
                        />
                    </Col>
                  </FormGroup>
                </Col>
              </Row>
            </div> }

            <FormGroup row={true}>
              <Label 
                for="profileForm__passportfiles" 
                sm={5} 
                className="profileForm__label">
                Прикрепите скан/фото паспорта
              </Label>
              <Col sm={7}>
                <Field
                  name="files[passport]"
                  component={renderUpload}
                  validate={[required]}
                  >
                  <span className='d-none d-sm-inline d-md-inline d-lg-inline'>Прикрепить файл</span>
                  <span className='d-inline d-sm-none d-md-none d-lg-none'>Прикрепите скан/фото паспорта</span>
                </Field>
              </Col>
            </FormGroup>

            <FormGroup row={true}>
              <Col sm={5}>
                <legend className="profileForm__label profileForm__label">
                  У вас есть трудовая книжка?
                </legend>
              </Col>
              <Col sm={7}>
                <div className="form-check-inline h-100">

                  <Field
                    component={renderCheckbox}
                    className="profileForm__radio profileForm__radio_inline"
                    name="employmenthistory"
                    type="radio"
                    value="true"
                    validate={[required]}
                    >
                      Да
                    </Field>
                  <Field
                    component={renderCheckbox}
                    className="profileForm__radio profileForm__radio_inline"
                    name="employmenthistory"
                    type="radio"
                    value="false"
                    validate={[required]}
                    >
                      Нет
                    </Field>
                </div>
              </Col>
            </FormGroup>
            { employmentHistory === "true" && 
              <FormGroup row={true}>
                <Label 
                  for="profileForm__employmenthistoryfiles" 
                  sm={5} 
                  className="profileForm__label">
                  Прикрепите скан/фото ТК
                </Label>
                <Col sm={7}>
                  <Field
                    name="files[employment]"
                    component={renderUpload}
                    validate={[required]}
                    >
                    <span className='d-none d-sm-inline d-md-inline d-lg-inline'>Прикрепить файл</span>
                    <span className='d-inline d-sm-none d-md-none d-lg-none'>Прикрепите скан/фото ТК</span>
                  </Field>
                </Col>
              </FormGroup>
            }

            <FormGroup row={true}>
              <Label 
                for="profileForm__inn" 
                sm={5} 
                className="profileForm__label">
                ИНН
              </Label>
              <Col sm={7}>
                <Field 
                  className="form-control form-inn" 
                  component={renderField} 
                  type="text" 
                  name="inn" 
                  id="profileForm__inn" 
                  validate={[required, minNum(12)]}
                  style={{width: '200px'}}
                  />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label 
                for="profileForm__innfiles" 
                sm={5} 
                className="profileForm__label">
                Прикрепите скан/фото ИНН
              </Label>
              <Col sm={7}>
                <Field
                  name="files[inn]"
                  component={renderUpload}
                  validate={[required]}
                  >
                  <span className='d-none d-sm-inline d-md-inline d-lg-inline'>Прикрепить файл</span>
                  <span className='d-inline d-sm-none d-md-none d-lg-none'>Прикрепите скан/фото ИНН</span>
                </Field>
              </Col>
            </FormGroup>

            <FormGroup row={true}>
              <Label 
                for="profileForm__snils" 
                sm={5} 
                className="profileForm__label">
                СНИЛС
              </Label>
              <Col sm={7}>
                <Field 
                  className="form-control form-snils" 
                  component={renderField} 
                  type="text" 
                  name="snils" 
                  id="profileForm__snils" 
                  validate={[required, minNum(11)]}
                  style={{width: '200px'}}
                  />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label 
                for="profileForm__snilsfiles" 
                sm={5} 
                className="profileForm__label">
                Прикрепите скан/фото СНИЛС
              </Label>
              <Col sm={7}>
                <Field
                  name="files[snils]"
                  component={renderUpload}
                  validate={[required]}
                  >
                  <span className='d-none d-sm-inline d-md-inline d-lg-inline'>Прикрепить файл</span>
                  <span className='dinline d-sm-none d-md-none d-lg-none'>Прикрепите скан/фото СНИЛС</span>
                </Field>
              </Col>
            </FormGroup>

            <FormGroup row={true}>
              <Label 
                for="profileForm__phone" 
                sm={5} 
                className="profileForm__label">
                Номер мобильного телефона
              </Label>
              <Col sm={7}>
                <Field 
                  className="form-control form-phone" 
                  component={renderField} 
                  type="text" 
                  name="phone" 
                  id="profileForm__phone" 
                  validate={[required, minNum(11)]}
                  style={{width: '200px'}}
                  />
              </Col>
            </FormGroup>

            <FormGroup row={true}>
              <Label 
                for="profileForm__email" 
                sm={5} 
                className="profileForm__label">
                E-mail
              </Label>
              <Col sm={7}>
                <Field 
                  className="form-control" 
                  component={renderField} 
                  type="text" 
                  name="email" 
                  id="profileForm__email"
                  validate={[required, email]}
                  />
              </Col>
            </FormGroup>

            <FormGroup row={true}>
              <Label
                form="profileForm__army"
                sm={5}
                className="profileForm__label">
                Вы военнобязанный?  
              </Label>
              <Col sm={7}>
                <div className="form-check-inline h-100">

                  <Field
                    component={renderCheckbox}
                    className="profileForm__radio profileForm__radio_inline"
                    name="militaryliable"
                    type="radio"
                    value="true"
                    validate={[required]}
                    >
                      Да
                    </Field>
                  <Field
                    component={renderCheckbox}
                    className="profileForm__radio profileForm__radio_inline"
                    name="militaryliable"
                    type="radio"
                    value="false"
                    validate={[required]}
                    >
                      Нет
                    </Field>
                </div>
              </Col>
            </FormGroup>

            {
              militaryLiable === "true" && 
              <FormGroup row={true}>
                <Label 
                  for="profileForm__militaryliablefiles" 
                  sm={5} 
                  className="profileForm__label">
                  Прикрепите скан/фото ВБ
                </Label>
                <Col sm={7}>
                  <Field
                    name="files[military]"
                    component={renderUpload}
                    validate={[required]}
                    >
                    <span className='d-none d-sm-inline d-md-inline d-lg-inline'>Прикрепить файл</span>
                    <span className='dinline d-sm-none d-md-none d-lg-none'>Прикрепите скан/фото ВБ</span>
                  </Field>
                </Col>
              </FormGroup>
            }
            
            <FieldArray 
              name="places" 
              component={renderPlaces} />  

            </div>
            }

            <FormGroup className="text-center">
              <Button color="primary" className="btn_brand btn_xlg" type="submit" disabled={submitting}>Отправить</Button>
            </FormGroup>
            <FormGroup className="text-center">
              <Field
                component={renderCheckbox}
                type="checkbox" 
                name="agree" 
                id="profileForm__agree" 
                className="profileForm__checkbox"
                style={{maxWidth: '410px', textAlign: 'left', fontSize: '14px'}}
                validate={[required]}
                >
                <div>Настоящим подтверждаю, что ознакомлен с </div>
                <a href="javascript:void(0)" onClick={props.toggle}>Политикой обработки персональных данных</a>
                <div>, и принимаю их</div>
                </Field>
            </FormGroup>
          </Form>
        </Col>
      </Row>
      <ModalPolitic 
        isOpen={props.modal} 
        toggle={props.toggle} />
    </Container>
  )
}*/

export default class WizardForm extends React.Component {
  constructor(props) {
    super(props)
    this.nextPage = this.nextPage.bind(this)
    this.previousPage = this.previousPage.bind(this)
    this.state = {
      page: 1
    }
  }
  nextPage() {
    this.setState({ page: this.state.page + 1 })
  }

  previousPage() {
    this.setState({ page: this.state.page - 1 })
  }

  render() {
    const { onSubmit } = this.props
    const { page } = this.state
    return (<div>
        {page === 1 && <MarketSelect onSubmit={this.nextPage}/>}
        {/* {page === 2 && <WizardFormSecondPage previousPage={this.previousPage} onSubmit={this.nextPage}/>} */}
        {page === 2 && <ProfileFormComponent previousPage={this.previousPage} onSubmit={onSubmit}/>}
      </div>
    )
  }
}

