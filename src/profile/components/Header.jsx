import * as React from 'react';
import { Container } from 'reactstrap';
import './_header.scss';

import logo from '../../assets/images/logo.svg';
import people from '../../assets/images/people.png';

const Header = (props) => {
  const { className } = props;
  return (
    <header className={`header ${className}`}>
      <Container className="position-relative">
        <img src={logo} className="header__logo" alt="logo" />
        <h1 className="header__title">Работай, когда удобно — зарабатывай, сколько хочешь в компании №1 в России</h1>
      </Container>
      <img src={people} className="header__people" alt="people" />
    </header>
  )
}

export default Header
