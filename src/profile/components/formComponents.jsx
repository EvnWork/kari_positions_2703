import * as React from 'react';
/* import { 
  // File, 
  FilePond, 
  registerPlugin 
} from 'react-filepond' */

import { Field } from 'redux-form'

import { 
  Button, 
  Col, 
  // Container, 
  // Form, 
  FormGroup,
  Label, 
  // Row,
} from 'reactstrap';

import { Russian } from "flatpickr/dist/l10n/ru.js";
import * as moment from 'moment';
import Flatpickr from 'react-flatpickr';

import Dropzone from 'react-dropzone';

import { 
  required, 
  // email, 
  // minNum 
} from './form/validation'

import svg from "../../assets/images/icons/attach.svg"
import remove from "../../assets/images/icons/close.svg"
import add from "../../assets/images/icons/add.svg"
// import { consoleTestResultHandler } from 'tslint/lib/test';

const renderCheckbox = ({
  input,
  about,
  type,
  style,
  className,
  children,
  meta: { touched, error, warning }
}) => {
  let errorClass = ( touched && error ) ? ' error' : '';
  return (
    <React.Fragment>
      <label 
        className={`${className} ${errorClass}`}
        style={style} >
        <input 
          {...input} 
          type={type} />
        {' '}
        <div className="wrap">
          {React.Children.map(children, (child, i) => {
            return child
          })}
        </div>
      </label>
    </React.Fragment>
  )
}

const renderField = ({
  input,
  label,
  pattern,
  type,
  style,
  className,
  placeholder,
  meta: { touched, error, warning }
}) => {
  let errorClass = ( touched && error ) ? ' error' : '';
  return (
    <React.Fragment>
    <input type={type} {...input} pattern={pattern} className={`${className} ${errorClass}`} style={style} placeholder={placeholder} />
      <div className="text-right text-error" style={style}>
        {
          touched 
          && 
          (
            (error && <span>{error}</span>) 
            || 
            (warning && <span>{warning}</span>)
          )
        }
      </div>
    </React.Fragment>
  )
}

const renderDatePicker = ({
  input,
  label,
  type,
  options,
  style,
  className,
  mode,
  meta: { touched, error, warning }
}) => {
  let errorClass = ( touched && error ) ? ' error' : '';
  let maxDate = ( input.name === 'bday' ) ? moment().subtract(18, "years").format("DD.MM.YYYY") : moment().format("DD.MM.YYYY")
  const { 
    onChange, 
    // value, 
    name 
  } = input;

  // console.log(input);
  const newOptions = {
    ...options, 
    maxDate : maxDate,
    locale : Russian,
  }
  // console.log(touched, error, warning);
  return (
    <React.Fragment>
      <Flatpickr 
        style={style}
        className={`${className} ${errorClass}`}
        {...input}
        // value={!value ? null : new Date(value)}
        onChange={onChange}
        name={name}
        options={newOptions}
        />
        <div className="text-right text-error" style={style}>
        { 
          //error !== undefined ? <span>{error}</span> : ''
          touched 
          && 
          (
            (error && <span>{error}</span>) 
            || 
            (warning && <span>{warning}</span>)
          )
        }
      </div>
    </React.Fragment>
  )
}

const renderUpload = (field) => {
  const files = field.input.value;
  let errrorClass = (field.meta.touched && field.meta.error) ? ' error' : ''
  /* TODO: Доработать удаление файлов */
  const removeImg = (arr, item) => {
    console.log(arr, item, arr.indexOf(item));
    const index = arr.indexOf(item)
    if (index > -1) {
      arr.splice(index, 1)
      // (dispatch) => dispatch(change(field.meta.form, field.name, 'Hello world'))
    }
    // return field.input.onChange(arr)
  }
  return (
    <React.Fragment>
      <Dropzone
        multiple
        //{...field.input}
        className={`dropzone ${errrorClass}`}
        accept="image/*"
        activeStyle={{backgroundColor: '#DDE9F9'}}
        name={field.name}
        onDrop={( filesToUpload, e ) => {
          let fileArr = field.input.value
          console.log(fileArr, filesToUpload)

          // let oldArr = []
          // let newArr = []
          // let iArr = []

          // if (fileArr.length > 0) {
          //   fileArr.map((item, i) => oldArr.push(item.name))
          //   filesToUpload.map((item, i) => newArr.push(item.name))
          //   newArr.map((item, i) => {
          //     console.log(i, oldArr.includes(item))
          //     if ( oldArr.includes(item) ) {
          //       iArr.push(i)
          //     }
          //   })
          // }
          // console.log(iArr)
          // fileArr.map((item, i) => console.log(item))

          const files = [...fileArr, ...filesToUpload]
          console.log([...new Set([...fileArr, ...filesToUpload])])

          console.log(files)


          if ( files.length < 10 ) {
            field.input.onChange(files)
          } else {
            files.splice(10, 99)
            field.input.onChange(files)
          }
        }}
      >
        <div className="dropzone__label">
          <img src={svg} alt="attach"/> {field.children}
        </div>
      </Dropzone>
        <div className="text-right text-error" style={field.style}>
          {field.meta.touched &&
            field.meta.error &&
            <span className="error">{field.meta.error}</span>}
        </div>
        {files && Array.isArray(files) && (
          <ul className="dropzone__list">
            { 
              files.map((file, i) => {
                return (
                  <li key={i} className="dropzone__item">
                    <div className="dropzone__img">
                      <img src={file.preview} alt={file.name} />
                    </div>
                    <button className="dropzone__remove" onClick={ () => removeImg(files, file) }>
                      <img src={remove} alt={remove} />
                    </button>
                      {/* <button onClick={ field.removeFile }>Жмякуй</button> */}
                  </li>
                )}
              ) 
            }
          </ul>
        )}
    </React.Fragment>
  );
}

const renderPlaces = ({ 
  fields, 
  meta: { error, submitFailed } 
}) => (
  <React.Fragment>
    <FormGroup row={true}>
      <Label 
        for="profileForm__places" 
        sm={5} 
        className="profileForm__label profileForm__label_medium">
        Укажите места работы <br className="d-xs-none" /> за последние 24 месяца
      </Label>
      <Col sm={7}>
        <Button 
          size="lg"
          type="button" 
          onClick={() => fields.push({})}>
          <img src={add} alt="add" /> <span>Добавить место</span>
        </Button>
        {submitFailed && error && <span>{error}</span>}
      </Col>
    </FormGroup>
    {fields.map((member, index) => (
      <React.Fragment key={index}>
        <FormGroup row={true}>
          <Label 
            for={`profileForm__places-name-${member}`} 
            sm={5} 
            className="profileForm__label">
            Название организации
          </Label>
          <Col sm={7}>
            <Field
              name={`${member}.name`}
              id={`profileForm__places-name-${member}`} 
              className="form-control"
              type="text"
              component={renderField}
              validate={[required]}
               />
          </Col>
        </FormGroup>
        <FormGroup row={true}>
          <Label 
            for={`profileForm__places-position-${member}`} 
            sm={5} 
            className="profileForm__label">
            Должность
          </Label>
          <Col sm={7}>
            <Field
              name={`${member}.position`}
              id={`profileForm__places-position-${member}`}
              className="form-control"
              type="text"
              component={renderField} 
              validate={[required]}
              />
          </Col>
        </FormGroup>
        <FormGroup row={true}>
          <Label 
            for={`profileForm__places-workstart-${member}`} 
            sm={5} 
            className="profileForm__label">
            Период работы
          </Label>
          <Col sm={7}>
            <Field
              name={`${member}.worktime`}
              id={`profileForm__places-worktime-${member}`}
              className="form-control"
              style={{maxWidth: '100%'}}
              type="text"
              options={{
                dateFormat : "d.m.Y",
                minDate : '01.01.1901',
                mode : 'range'
              }}
              component={renderDatePicker} 
              validate={[required]}
              />
          </Col>
        </FormGroup>
        <FormGroup row={true}>
          <Label 
            for={`profileForm__places-func-${member}`} 
            sm={5} 
            className="profileForm__label">
            Функционал
          </Label>
          <Col sm={7}>
            <Field
              name={`${member}.func`}
              id={`profileForm__places-func-${member}`}
              className="form-control"
              component="textarea" 
              validate={[required]}
              />
          </Col>
        </FormGroup>
        <FormGroup row={true}>
          <Col 
            sm={{size: '7', offset: '5'}} 
            style={{textAlign : "right"}}>
            <Button
              color="link"
              className="remove-place"
              type="button"
              title="Удалить место"
              onClick={() => fields.remove(index)}>
              Удалить место
            </Button>
          </Col>
        </FormGroup>
      </React.Fragment>
    ))}
  </React.Fragment>
)

export {
  renderCheckbox, 
  renderDatePicker, 
  renderField, 
  renderPlaces,
  renderUpload,
};
