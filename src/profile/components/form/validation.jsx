export const required = (value) => (value || typeof value === 'number' ? undefined : 'Обязательное поле')
export const email = (value) => {
  return (
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
      ? 'Неправильный формат'
      : undefined
  )
}
export const minNum = (min) => (value) => {
  return ( 
    value && value.replace(/[^0-9 ]/g, '').length < min 
      ? `Должно быть минимум ${min} чисел` 
      : undefined
  )
}
export const maxNum = (min) => (value) => {
  return ( 
    value && value.replace(/[^0-9 ]/g, '').length > min 
      ? `Должно быть максимум ${min} чисел` 
      : undefined
  )
}
/* const minLength = (min:number) => (value?:any) =>
  value && value.length < min ? `Минимум ${min} символов` : undefined
const onlyNum = (value:any) => {
  // value = value.replace(/\s+/g, '');
  value && isNaN(Number(value)) ? 'Должны быть только цифры' : undefined
} */
