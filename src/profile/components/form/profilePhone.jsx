import * as React from 'react';
import { 
  Button, 
  Col, 
  Container, 
  Form, 
  FormGroup,
  Label, 
  Row,
} from 'reactstrap';
import {
  // renderPlaces,
  renderField, 
  renderCheckbox, 
  renderDatePicker,
  // renderUpload
} from '../formComponents';
import {
  Field, 
  // FieldArray,
  formValueSelector, 
  reduxForm, 
  // reset,
} from 'redux-form';

import { connect } from 'react-redux'
import IMask from 'imask';
import { 
  required, 
  // email, 
  minNum 
} from './validation'
import server from '../../../serverConfig'
import axios from 'axios';


const maskList = {
  phone : {
    mask: '+0 (000) 000-00-00',
    lazy: false, 
    placeholderChar : '#'
  },
  code : {
    mask: '0000',
    lazy: false, 
    placeholderChar : '#'
  },
}

const setIMask = (selector, mask) => {
  const inputs = document.querySelectorAll(selector)
  inputs.forEach((item) => {
    new IMask(item,mask)
  })
}

const checkPhone = async (phone, code = '') => {
  try {
    return await axios.post(
      `${server.url}/api/v1/questionnaire/phoneconfirm`,
      // `/api/v1/questionnaire/phoneconfirm`,
      {
        phone : phone,
        code : code
      },
      // {
      //   headers: {
      //     'Authorization': `${server.token}`
      //   }
      // }
    );
  } catch (error) {
    // console.log("Bad postEvents: ", error);
    return error.status
  }
}

class PhoneValidate extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      phoneCode : false,
      confirmCode : false
    }
  }

  componentDidMount(){
    setIMask('.form-phone', maskList.phone)
  }
  componentDidUpdate(){
    setIMask('.form-code', maskList.code)
  }

  confirmPhone = async (phone, code = '') => {
    let callback = await checkPhone(phone.replace(/\D/g,''), code)

    // console.log(callback);

    if (phone.replace(/\D/g,'').length === 11){
      this.setState(previousState => ({ phoneCode : !previousState.phoneCode }))

      if (code && callback.status === 200) {
        this.setState(previousState => ({ confirmCode : !previousState.phoneCode }))
      }
    }
  }

  render() {

    return(

      <Container>
        <Row>
          <Col className="form-wrapper" sm="12" lg={{ size: 10, offset: 1 }}>
            <Form 
              model="user"
              encType="multipart/form-data"
              className="profileForm" 
              onSubmit={ this.props.handleSubmit }>
              {/* onSubmit={this.state.confirmCode ? this.props.handleSubmit : (e) => { e.preventDefault(); return false; }}> */}

              <legend className="profileForm__title text-center">Подтверждение кандидата</legend>

              <FormGroup row={true}>
                <Col sm={5}>
                  <legend className="profileForm__label profileForm__label_medium">
                    Вы гражданин России?
                  </legend>
                </Col>
                <Col sm={7}>
                  <div className="form-check-inline h-100">
                    <Field
                      component={renderCheckbox}
                      className="profileForm__radio profileForm__radio_inline"
                      name="citizenship"
                      type="radio"
                      value="true"
                      validate={[required]}
                      >
                        Да
                      </Field>
                    <Field
                      component={renderCheckbox}
                      className="profileForm__radio profileForm__radio_inline"
                      name="citizenship"
                      type="radio"
                      value="false"
                      validate={[required]}
                      >
                        Нет
                      </Field>
                  </div>
                </Col>
              </FormGroup>

              <FormGroup row={true}>
                <Label for="profileForm__bday" sm={5} className="profileForm__label">Дата рождения</Label>
                <Col sm={7}>
                  <Field 
                    name="bday"
                    className="form-control" 
                    style={{maxWidth: '133px'}}
                    component={renderDatePicker} 
                    options={{
                      dateFormat : "d.m.Y",
                      minDate : '01.01.1901',
                      mode : 'single'
                    }}
                    validate={[required]} 
                    />
                </Col>
              </FormGroup>


              <FormGroup row={true}>
                <Label 
                  for="profileForm__phone" 
                  sm={5} 
                  className="profileForm__label">
                  Номер мобильного телефона
                </Label>
                <Col sm={7}>
                  <Field 
                    className="form-control form-phone" 
                    component={renderField} 
                    type="text" 
                    name="phone" 
                    id="profileForm__phone" 
                    validate={[required, minNum(11)]}
                    style={{width: '200px'}}
                    />
                </Col>
              </FormGroup>

              { this.props.phone && !this.state.phoneCode && !this.state.confirmCode &&
                <FormGroup className="text-center">
                  <Button color="primary" className="btn_brand btn_xlg" type="button" onClick={() => this.confirmPhone(this.props.phone)}>Подтвердить номер</Button>
                </FormGroup> 
              }

              { this.props.phone && this.state.phoneCode && 
                <React.Fragment>
                  <FormGroup row={true}>
                    <Label 
                      for="profileForm__code" 
                      sm={5} 
                      className="profileForm__label">
                      Код подтверждения
                    </Label>
                    <Col sm={7}>
                      <Field 
                        className="form-control form-code" 
                        component={renderField} 
                        type="text" 
                        name="code" 
                        id="profileForm__code" 
                        validate={[required, minNum(4)]}
                        style={{width: '200px'}}
                        />
                    </Col>
                  </FormGroup>
                  <FormGroup className="text-center">
                    <Button color="primary" className="btn_brand btn_xlg" type="button" onClick={() => this.confirmPhone(this.props.phone, this.props.code)}>Подтвердить</Button>
                  </FormGroup> 
                </React.Fragment>
              }

              {/* <FormGroup className="text-center">
                  <Button color="primary" className="btn_brand btn_xlg" type="submit" disabled={this.props.submitting}>Далее</Button>
              </FormGroup> */}

              { this.props.phone && this.props.code && this.state.confirmCode && <FormGroup className="text-center">
                <Button color="primary" className="btn_brand btn_xlg" type="submit" disabled={this.props.submitting}>Далее</Button>
              </FormGroup> }
            </Form>
          </Col>
        </Row>
      </Container>
    )
  }
}

const selector = formValueSelector('profile')
PhoneValidate = connect(
  state => {
    const phone = selector(state, 'phone')
    const code = selector(state, 'code')
    return {
      phone, 
      code
    }
  }
)(PhoneValidate)

export default reduxForm({
  form: 'profile',
  destroyOnUnmount: false,
  multipartForm : true,
})(PhoneValidate)