import * as React from 'react';
import { 
  Button, 
  Col, 
  Container, 
  Form, 
  FormGroup,
  Label, 
  Row,
} from 'reactstrap';
import {
  renderPlaces,
  renderField, 
  renderCheckbox, 
  // renderDatePicker,
  renderUpload
} from '../formComponents';
import {
  Field, 
  FieldArray,
  formValueSelector, 
  reduxForm, 
  // reset,
} from 'redux-form';
import { connect } from 'react-redux'
import IMask from 'imask';
import { required, email, minNum } from './validation'

import ModalPolitic from './modalPolitic'

const maskList = {
  passport : {
    mask : '0000 000000',
    lazy: false, 
    placeholderChar : '#'
  },
  phone : {
    mask: '+0 (000) 000-00-00',
    lazy: false, 
    placeholderChar : '#'
  },
  snils : {
    mask: '000-000-000 00',
    lazy: false,
    placeholderChar : '#'
  },
  inn : {
    mask: '000000000000',
    lazy: false, 
    placeholderChar : '#'
  },
}

const setIMask = (selector, mask) => {
  const inputs = document.querySelectorAll(selector)
  inputs.forEach((item) => {
    new IMask(item,mask)
  })
}

let ProfileFormComponent = (props) => {
  const {
    actualPlace,
    employmentHistory,
    handleSubmit, 
    // isRussian, 
    militaryLiable,
    submitting, 
  } = props;

  setIMask('.form-inn', maskList.inn);
  setIMask('.form-passport', maskList.passport);
  setIMask('.form-phone', maskList.phone);
  setIMask('.form-snils', maskList.snils);

  return(
    <Container>
      <Row>
        <Col className="form-wrapper" sm="12" lg={{ size: 10, offset: 1 }}>
          <Form 
            model="user"
            encType="multipart/form-data"
            className="profileForm" 
            onSubmit={handleSubmit}>
            <legend className="profileForm__title text-center">Давайте знакомиться :)</legend>

            <FormGroup row={true}>
              <Label for="profileForm__lastname" sm={5} className="profileForm__label">Фамилия</Label>
              <Col sm={7}>
                <Field 
                  className="form-control" 
                  component={renderField} 
                  type="text" 
                  name="lastname" 
                  validate={[required]}
                  id="profileForm__lastname" />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label for="profileForm__firstname" sm={5} className="profileForm__label">Имя</Label>
              <Col sm={7}>
                <Field 
                  className="form-control" 
                  component={renderField} 
                  type="text" 
                  name="firstname" 
                  validate={[required]}
                  id="profileForm__firstname" />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label for="profileForm__secondname" sm={5} className="profileForm__label">Отчество</Label>
              <Col sm={7}>
                <Field 
                  className="form-control" 
                  component={renderField} 
                  type="text" 
                  name="secondname" 
                  validate={[required]}
                  id="profileForm__secondname" />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Col sm={5}>
                <legend className="profileForm__label profileForm__label">
                  Пол
                </legend>
              </Col>
              <Col sm={7}>
                <div className="form-check-inline h-100">
                  <Field
                    component={renderCheckbox}
                    className="profileForm__radio profileForm__radio_inline"
                    name="sex"
                    type="radio"
                    value="male"
                    validate={[required]}
                    >
                      Мужской
                    </Field>
                  <Field
                    component={renderCheckbox}
                    className="profileForm__radio profileForm__radio_inline"
                    name="sex"
                    type="radio"
                    value="female"
                    validate={[required]}
                    >
                      Женский
                    </Field>
                </div>
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label for="profileForm__passportnum" sm={5} className="profileForm__label">Серия и номер паспорта</Label>
              <Col sm={7}>
                <Field 
                  className="form-control form-passport" 
                  component={renderField} 
                  type="text" 
                  name="passportnum" 
                  validate={[required, minNum(10)]}
                  id="profileForm__passportnum" 
                  style={{maxWidth: '154px'}} />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label 
                for="profileForm__bplace" 
                sm={5} 
                className="profileForm__label">
                Место рождения
              </Label>
              <Col sm={7}>
                <Field 
                  className="form-control" 
                  component={renderField} 
                  type="text" 
                  name="bplace" 
                  validate={[required]}
                  id="profileForm__bplace" />
              </Col>
            </FormGroup>
            <FormGroup>
              <legend className="profileForm__label profileForm__label_medium">Регистрация</legend>
            </FormGroup>
            <FormGroup row={true}>
              <Label 
                for="profileForm__regcity" 
                sm={5} 
                className="profileForm__label">
                Город
              </Label>
              <Col sm={7}>
                <Field 
                  className="form-control" 
                  component={renderField} 
                  type="text" 
                  name="regcity" 
                  validate={[required]}
                  id="profileForm__regcity" />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label 
                for="profileForm__regstreet" 
                sm={5} 
                className="profileForm__label">
                Улица
              </Label>
              <Col sm={7}>
                <Field 
                  className="form-control" 
                  component={renderField} 
                  type="text" 
                  name="regstreet" 
                  validate={[required]}
                  id="profileForm__regstreet" />
              </Col>
            </FormGroup>

            <Row>
              <Col xs={4} sm={12}>
                <FormGroup row={true}>
                  <Label 
                    for="profileForm__regbuild" 
                    sm={5} 
                    className="profileForm__label">
                    Дом
                  </Label>
                  <Col sm={7}>
                    <Field 
                      className="form-control" 
                      component={renderField} 
                      type="text" 
                      name="regbuild" 
                      validate={[required]}
                      id="profileForm__regbuild" 
                      style={{maxWidth: '133px'}} />
                  </Col>
                </FormGroup>
              </Col>
              <Col xs={4} sm={12}>
                <FormGroup row={true}>
                  <Label 
                    for="profileForm__regcorps" 
                    sm={5} 
                    className="profileForm__label">
                    Корпус
                  </Label>
                  <Col sm={7}>
                    <Field 
                      className="form-control" 
                      component={renderField} 
                      type="text" 
                      name="regcorps" 
                      id="profileForm__regcorps" 
                      style={{maxWidth: '133px'}} />
                  </Col>
                </FormGroup>
              </Col>
              <Col xs={4} sm={12}>
                <FormGroup row={true}>
                  <Label 
                    for="profileForm__regapartment" 
                    sm={5} 
                    className="profileForm__label">
                    Квартира
                  </Label>
                  <Col sm={7}>
                    <Field 
                      className="form-control" 
                      component={renderField} 
                      type="text" 
                      name="regapartment" 
                      id="profileForm__regapartment" 
                      style={{maxWidth: '133px'}} />
                  </Col>
                </FormGroup>
              </Col>
            </Row>
            
            <FormGroup row={true}>
              <Col sm={5}>
                <legend 
                  className="profileForm__label profileForm__label_medium">
                  Фактическое место проживания
                </legend>
              </Col>
              <Col sm={7}>
                <Field
                  component={renderCheckbox}
                  className="profileForm__checkbox"
                  name="actualplace"
                  type="checkbox"
                  value="true">
                    Совпадает с адресом регистрации
                  </Field>
              </Col>
            </FormGroup>
            
            { !actualPlace && <div>
              <FormGroup row={true}>
                <Label 
                  for="profileForm__actualcity" 
                  sm={5} 
                  className="profileForm__label">
                  Город
                </Label>
                <Col sm={7}>
                  <Field 
                    className="form-control" 
                    component={renderField} 
                    type="text" 
                    name="actualcity" 
                    id="profileForm__actualcity" />
                </Col>
              </FormGroup>

              <FormGroup row={true}>
                <Label 
                  for="profileForm__actualstreet" 
                  sm={5} 
                  className="profileForm__label">
                  Улица
                </Label>
                <Col sm={7}>
                  <Field 
                    className="form-control" 
                    component={renderField} 
                    type="text" 
                    name="actualstreet" 
                    id="profileForm__actualstreet" />
                </Col>
              </FormGroup>
              
              <Row>
                <Col xs={4} sm={12}>
                  <FormGroup row={true}>
                    <Label 
                      for="profileForm__actualbuild" 
                      sm={5} 
                      className="profileForm__label">
                      Дом
                    </Label>
                    <Col sm={7}>
                      <Field 
                        className="form-control" 
                        component={renderField} 
                        type="text" 
                        name="actualbuild" 
                        id="profileForm__actualbuild" 
                        style={{maxWidth: '133px'}} />
                    </Col>
                  </FormGroup>
                </Col>
                <Col xs={4} sm={12}>
                  <FormGroup row={true}>
                    <Label 
                      for="profileForm__actualcorps" 
                      sm={5} 
                      className="profileForm__label">
                      Корпус
                    </Label>
                    <Col sm={7}>
                      <Field 
                        className="form-control" 
                        component={renderField} 
                        type="text" 
                        name="actualcorps" 
                        id="profileForm__actualcorps" 
                        style={{maxWidth: '133px'}} 
                        />
                    </Col>
                  </FormGroup>
                </Col>
                <Col xs={4} sm={12}>
                  <FormGroup row={true}>
                    <Label 
                      for="profileForm__actualapartment" 
                      sm={5} 
                      className="profileForm__label">
                      Квартира
                    </Label>
                    <Col sm={7}>
                      <Field 
                        className="form-control" 
                        component={renderField} 
                        type="text" 
                        name="actualapartment" 
                        id="profileForm__actualapartment" 
                        style={{maxWidth: '133px'}} 
                        />
                    </Col>
                  </FormGroup>
                </Col>
              </Row>
            </div> }

            <FormGroup row={true}>
              <Label 
                for="profileForm__passportfiles" 
                sm={5} 
                className="profileForm__label">
                Прикрепите скан/фото паспорта
              </Label>
              <Col sm={7}>
                {/* <Field
                  component={UploadFile}
                  about={""}
                  name="passportfiles" /> */}
                <Field
                  name="files[passport]"
                  component={renderUpload}
                  // removeFile={ () => console.log(value) }
                  validate={[required]}
                  >
                  <span className='d-none d-sm-inline d-md-inline d-lg-inline'>Прикрепить файл</span>
                  <span className='d-inline d-sm-none d-md-none d-lg-none'>Прикрепите скан/фото паспорта</span>
                </Field>
              </Col>
            </FormGroup>

            <FormGroup row={true}>
              <Col sm={5}>
                <legend className="profileForm__label profileForm__label">
                  У вас есть трудовая книжка?
                </legend>
              </Col>
              <Col sm={7}>
                <div className="form-check-inline h-100">

                  <Field
                    component={renderCheckbox}
                    className="profileForm__radio profileForm__radio_inline"
                    name="employmenthistory"
                    type="radio"
                    value="true"
                    validate={[required]}
                    >
                      Да
                    </Field>
                  <Field
                    component={renderCheckbox}
                    className="profileForm__radio profileForm__radio_inline"
                    name="employmenthistory"
                    type="radio"
                    value="false"
                    validate={[required]}
                    >
                      Нет
                    </Field>
                </div>
              </Col>
            </FormGroup>
            { employmentHistory === "true" && 
              <FormGroup row={true}>
                <Label 
                  for="profileForm__employmenthistoryfiles" 
                  sm={5} 
                  className="profileForm__label">
                  Прикрепите скан/фото ТК
                </Label>
                <Col sm={7}>
                  {/* <Field
                    component={UploadFile}
                    about={""}
                    name="employmenthistoryfiles" /> */}
                  <Field
                    name="files[employment]"
                    component={renderUpload}
                    validate={[required]}
                    >
                    <span className='d-none d-sm-inline d-md-inline d-lg-inline'>Прикрепить файл</span>
                    <span className='d-inline d-sm-none d-md-none d-lg-none'>Прикрепите скан/фото ТК</span>
                  </Field>
                </Col>
              </FormGroup>
            }

            <FormGroup row={true}>
              <Label 
                for="profileForm__inn" 
                sm={5} 
                className="profileForm__label">
                ИНН
              </Label>
              <Col sm={7}>
                <Field 
                  className="form-control form-inn" 
                  component={renderField} 
                  type="text" 
                  name="inn" 
                  id="profileForm__inn" 
                  validate={[required, minNum(12)]}
                  style={{width: '200px'}}
                  />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label 
                for="profileForm__innfiles" 
                sm={5} 
                className="profileForm__label">
                Прикрепите скан/фото ИНН
              </Label>
              <Col sm={7}>
                {/* <Field
                    component={UploadFile}
                    about={"<span class='d-none d-sm-inline d-md-inline d-lg-inline'>Прикрепить файл</span><span class='d-inline d-sm-none d-md-none d-lg-none'>Прикрепите скан/фото ИНН</span>"}
                    name="innfiles" /> */}
                <Field
                  name="files[inn]"
                  component={renderUpload}
                  validate={[required]}
                  >
                  <span className='d-none d-sm-inline d-md-inline d-lg-inline'>Прикрепить файл</span>
                  <span className='d-inline d-sm-none d-md-none d-lg-none'>Прикрепите скан/фото ИНН</span>
                </Field>
              </Col>
            </FormGroup>

            <FormGroup row={true}>
              <Label 
                for="profileForm__snils" 
                sm={5} 
                className="profileForm__label">
                СНИЛС
              </Label>
              <Col sm={7}>
                <Field 
                  className="form-control form-snils" 
                  component={renderField} 
                  type="text" 
                  name="snils" 
                  id="profileForm__snils" 
                  validate={[required, minNum(11)]}
                  style={{width: '200px'}}
                  />
              </Col>
            </FormGroup>
            <FormGroup row={true}>
              <Label 
                for="profileForm__snilsfiles" 
                sm={5} 
                className="profileForm__label">
                Прикрепите скан/фото СНИЛС
              </Label>
              <Col sm={7}>
                {/* <Field
                    component={UploadFile}
                    about={"<span class='d-none d-sm-inline d-md-inline d-lg-inline'>Прикрепить файл</span><span class='d-inline d-sm-none d-md-none d-lg-none'>Прикрепите скан/фото СНИЛС</span>"}
                    name="snilsfiles" /> */}
                <Field
                  name="files[snils]"
                  component={renderUpload}
                  validate={[required]}
                  >
                  <span className='d-none d-sm-inline d-md-inline d-lg-inline'>Прикрепить файл</span>
                  <span className='dinline d-sm-none d-md-none d-lg-none'>Прикрепите скан/фото СНИЛС</span>
                </Field>
              </Col>
            </FormGroup>

            <FormGroup row={true}>
              <Label 
                for="profileForm__email" 
                sm={5} 
                className="profileForm__label">
                E-mail
              </Label>
              <Col sm={7}>
                <Field 
                  className="form-control" 
                  component={renderField} 
                  type="text" 
                  name="email" 
                  id="profileForm__email"
                  validate={[required, email]}
                  />
              </Col>
            </FormGroup>

            <FormGroup row={true}>
              <Label
                form="profileForm__army"
                sm={5}
                className="profileForm__label">
                Вы военнобязанный?  
              </Label>
              <Col sm={7}>
                <div className="form-check-inline h-100">

                  <Field
                    component={renderCheckbox}
                    className="profileForm__radio profileForm__radio_inline"
                    name="militaryliable"
                    type="radio"
                    value="true"
                    validate={[required]}
                    >
                      Да
                    </Field>
                  <Field
                    component={renderCheckbox}
                    className="profileForm__radio profileForm__radio_inline"
                    name="militaryliable"
                    type="radio"
                    value="false"
                    validate={[required]}
                    >
                      Нет
                    </Field>
                </div>
              </Col>
            </FormGroup>

            {
              militaryLiable === "true" && 
              <FormGroup row={true}>
                <Label 
                  for="profileForm__militaryliablefiles" 
                  sm={5} 
                  className="profileForm__label">
                  Прикрепите скан/фото ВБ
                </Label>
                <Col sm={7}>
                  {/* <Field
                      component={UploadFile}
                      about={"<span class='d-none d-sm-inline d-md-inline d-lg-inline'>Прикрепить файл</span><span class='d-inline d-sm-none d-md-none d-lg-none'>Прикрепите скан/фото СНИЛС</span>"}
                      name="snilsfiles" /> */}
                  <Field
                    name="files[military]"
                    component={renderUpload}
                    validate={[required]}
                    >
                    <span className='d-none d-sm-inline d-md-inline d-lg-inline'>Прикрепить файл</span>
                    <span className='dinline d-sm-none d-md-none d-lg-none'>Прикрепите скан/фото ВБ</span>
                  </Field>
                </Col>
              </FormGroup>
            }
            
            <FieldArray 
              name="places" 
              component={renderPlaces} />
          

            <FormGroup className="text-center">
              <Button color="primary" className="btn_brand btn_xlg" type="submit" disabled={submitting}>Отправить</Button>
            </FormGroup>
            <FormGroup className="text-center">
              <Field
                component={renderCheckbox}
                type="checkbox" 
                name="agree" 
                id="profileForm__agree" 
                className="profileForm__checkbox"
                style={{maxWidth: '410px', textAlign: 'left', fontSize: '14px'}}
                validate={[required]}
                >
                <div>Настоящим подтверждаю, что ознакомлен с </div>
                <a href="javascript:void(0)" onClick={props.toggle}>Политикой обработки персональных данных</a>
                <div>, и принимаю их</div>
                </Field>
            </FormGroup>
          </Form>
        </Col>
      </Row>
      <ModalPolitic 
        isOpen={props.modal} 
        toggle={props.toggle} />
    </Container>
  )
}

const selector = formValueSelector('profile')

ProfileFormComponent = reduxForm({
  form: 'profile',
  multipartForm : true,
})(ProfileFormComponent)

ProfileFormComponent = connect(
  state => {
    const isRussian = selector(state, 'citizenship')
    const actualPlace = selector(state, 'actualplace')
    const employmentHistory = selector(state, 'employmenthistory')
    const militaryLiable = selector(state, 'militaryliable')
    return {
      isRussian,
      actualPlace,
      employmentHistory,
      militaryLiable
    }
  }
)(ProfileFormComponent)

export default ProfileFormComponent