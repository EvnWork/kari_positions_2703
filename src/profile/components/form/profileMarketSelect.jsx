import * as React from 'react';
import { 
  Button, 
  Col, 
  Container, 
  Form, 
  FormGroup,
  Label, 
  Row,
} from 'reactstrap';
import {
  // renderPlaces,
  renderField, 
  // renderCheckbox, 
  // renderDatePicker,
  // renderUpload
} from '../formComponents';
import {
  Field, 
  // FieldArray,
  formValueSelector, 
  reduxForm, 
  // reset,
} from 'redux-form';

import Preload from '../../../sugarComponents/Preloader'

import { connect } from 'react-redux'
// import IMask from 'imask';
import { 
  required, 
  // email, 
  // minNum 
} from './validation'
import axios from 'axios';
// import server from '../../../serverConfig';

const CitiesOptions = (props) => {
  // console.log(props);
  return props.data.map((item) => {
    return (
      <option value={`${item.cityId}`} key={item.cityId}>{item.name}</option>
    )
  })
}

const MarketOptions = (props) => {
  return props.data.map((item) => {
    return (
      <option value={`${item.IStoreId}`} key={item.IStoreId}>{item.param.mall ? `${item.param.mall}, ` : ''}{item.param.street}</option>
    )
  })
}

const PositionOptions = (props) => {
  return props.data.map((item, i) => {
    return (
      <option value={`${item}`} key={i}>{item}</option>
    )
  })
}

// const marketLits = async (cityId) => {
//   try {
//     return await axios.get(
//       // 'http://172.16.15.45:5000/api/v1/questionnaire',
//       `https://kari-dev.lad24.ru/cloud/eshop/stores?cityId=${cityId}`
//     );
//   } catch (error) {
//     // console.log("Bad postEvents: ", error);
//     return error.status
//   }
// }

class MarketSelect extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      cities : null,
      markets : null,
      positions : [
        'Директор Магазина',
        'Администратор Магазина',
        'Продавец ювелирных изделий',
        'Продавец Пьер Карден',
        'Продавец-кассир',
        'Продавец-консультант',
        'Продавец-парттаймер'
      ]
    }
  }

  async componentDidMount () {
    // const cities = await axios.get('https://kari-dev.lad24.ru/cloud/eshop/cities?countryId=2015ad55-40aa-496d-8695-e0ead2fa0cca');
    const cities = await axios.get('https://kari-dev.lad24.ru/cloud/eshop/cities');
    
    this.setState({
      cities : cities.data
    })
  }

  changeCity = async (e) => {
    let city = e.target.value
    let marketList = await axios.get(
      `https://kari-dev.lad24.ru/cloud/eshop/stores?cityId=${city}`
      // `https://kari-dev.lad24.ru/cloud/stores?active=true&cityId=6aff93a7-c053-406f-a1e4-ffd0f47d1157${city}`,
      // {
      //   headers: {
      //     'Authorization': `${server.lcfToken}`,
      //   }
      // }
    );
    // console.log(marketList);

    this.props.change('countryId', marketList.data[0].param.countryId)
    // this.props.change('regionAreaId', marketList.data.data[0].param.regionAreaId)

    this.setState({
      markets : marketList.data
    })
  }

  changeStore = (e) => {
    console.log(e.target.value);

    const { markets } = this.state

    if ( ['af704da5-9bcb7d04aace', 'a85ea20b-adcb-f1d5f2bb4957' ].indexOf(e.target.value) !== -1 ) {
      this.props.change('storeId', e.target.value)
    } else {
      for (let i in markets) {
        if (markets[i].IStoreId === e.target.value){
          this.props.change('storeId', markets[i].unitId)
          // this.props.change('uniteId', markets[i]._id)
          break;
        }
      }
    }
  }

  render() {

    const { cityId } = this.props

    if (this.state.cities !== null ) {
      
      return (

        <Container>
          <Row>
            <Col className="form-wrapper" sm="12" lg={{ size: 10, offset: 1 }}>
              <Form 
                model="user"
                encType="multipart/form-data"
                className="profileForm" 
                onSubmit={this.props.handleSubmit}>

                <FormGroup row={true}>
                  <Label 
                    for="profileForm__city" 
                    sm={5} 
                    className="profileForm__label">Укажите ваш город</Label>
                  <Col sm={7}>
                    <Field 
                      className="form-control" 
                      component="select"
                      name="cityId" 
                      onChange={this.changeCity}
                      validate={[required]}
                      id="profileForm__city">
                      <option value="" disabled selected> - Выберите город - </option>  
                      <CitiesOptions data={this.state.cities} />
                    </Field>
                  </Col>
                </FormGroup>

                { cityId && this.state.markets && <React.Fragment><FormGroup row={true}>
                  <Label 
                    for="profileForm__market" 
                    sm={5} 
                    className="profileForm__label">Адрес магазина</Label>
                  <Col sm={7}>
                    <Field 
                      className="form-control" 
                      component="select"
                      name="IStoreId" 
                      onChange={this.changeStore}
                      validate={[required]}
                      id="profileForm__market">
                      <option value="" disabled selected> - Выберите магазин - </option>  
                      <option value="af704da5-9bcb7d04aace"> - Тестовый магазин 1 - </option>  
                      <option value="a85ea20b-adcb-f1d5f2bb4957"> - Тестовый магазин 2 - </option>  
                      <MarketOptions data={this.state.markets} />
                    </Field>
                  </Col>
                </FormGroup><FormGroup row={true}>
                  <Label 
                    for="profileForm__position" 
                    sm={5} 
                    className="profileForm__label">Выберите должность</Label>
                  <Col sm={7}>
                    <Field 
                      className="form-control" 
                      component="select"
                      name="position" 
                      validate={[required]}
                      id="profileForm__position">
                      <option value="" disabled selected> - Выберите должность - </option>
                      <PositionOptions data={this.state.positions} /> 
                    </Field>
                  </Col>
                </FormGroup>

                <FormGroup className="text-center">
                  <Button color="primary" className="btn_brand btn_xlg" type="submit" disabled={this.props.submitting}>Далее</Button>
                </FormGroup>

                </React.Fragment> }

                <Field name="countryId" type="hidden" disabled component={renderField} className="form-control"  />
                {/* <Field name="regionAreaId" type="hidden" disabled component={renderField} className="form-control"  /> */}
                <Field name="storeId" type="hidden" disabled component={renderField} className="form-control"  />
                {/* <Field name="uniteId" type="hidden" disabled component={renderField} className="form-control"  /> */}

              </Form>
            </Col>
          </Row>
        </Container>
      )
    } else {
      return (
        <Preload />
      )
    }
  }
}

const selector = formValueSelector('profile')
MarketSelect = connect(
  state => {
    const cityId = selector(state, 'cityId')
    const text = selector(state, 'text')
    return {
      text,
      cityId,
    }
  }
)(MarketSelect)

export default reduxForm({
  form: 'profile',
  destroyOnUnmount: false,
  multipartForm : true,
})(MarketSelect)