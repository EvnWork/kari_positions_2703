const server = {
  // url : 'https://kari-recruiting-test.lad24.ru',
  // url : 'http://job.kari.com',
  url : window.location.hostname !== 'localhost' ? `${window.location.protocol}//${window.location.hostname}` : 'https://kari-recruiting-test.lad24.ru',
  token : 'Bearer testerToken',
  lcfToken : 'Bearer 52c93a2b-fdfc-4b4e-9802-21f5a60de233'
}

export default server